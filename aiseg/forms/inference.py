import os
import torch

from django import forms

from aiseg import constants
from aiseg.file_handling.utils import get_files
from aiseg.models import InferenceParameters


class InferenceInputForm(forms.ModelForm):
    class Meta:
        model = InferenceParameters
        fields = [
            'model_path',
            'data_folder',
            'device',
            'save_color',
            'log_thresh',
            'overlap',
            'weighting_strategy',
            'batch_size',
            'input_size',
            'input_depth',
        ]
        widgets = {
            'model_path': forms.TextInput(attrs={'class': 'form-control'}),
            'data_folder': forms.TextInput(attrs={'class': 'form-control'}),

            'device': forms.Select(attrs={'class': 'form-control'}),
            'save_color': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'log_thresh': forms.NumberInput(attrs={'class': 'form-control'}),
            'overlap': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'batch_size': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_size': forms.NumberInput(attrs={'class': 'form-control'}),
            'input_depth': forms.NumberInput(attrs={'class': 'form-control'}),

            'weighting_strategy': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        required_fields = [
            'model_path',
            'data_folder',
            'log_thresh',
            'overlap',
            'weighting_strategy',
        ]

        for field in self.fields:
            if field in required_fields:
                self.fields[field].required = True

    def __clean_field_dependent_fields(self, fcd, field, from_field_dependent_fields, fields_with_error):
        added_error = False
        if field in fcd and fcd[field]:
            for dependent_field in from_field_dependent_fields:
                if dependent_field in fields_with_error:
                    continue
                if dependent_field not in fcd or not fcd[dependent_field]:
                    self.add_error(
                        dependent_field,
                        f'You have specified the {field.replace("_", " ")}. '
                        f'You also need to specify a value for {dependent_field.replace("_", " ")}.'
                    )
                    added_error = True
        return added_error

    def __clean_batch_size_and_input_dimensions(self, fcd, fields_to_clean):
        fields_with_error = []
        for field in fields_to_clean:
            if field in fields_with_error:
                continue
            other_fields = [f for f in fields_to_clean if f != field]
            added_error = self.__clean_field_dependent_fields(fcd, field, other_fields, fields_with_error)
            if added_error:
                fields_with_error.append(field)


    def clean(self):
        fcd = super().clean()

        if not os.path.isfile(fcd['model_path']):
            self.add_error('model_path', 'The model path does not exist.')
        elif not fcd['model_path'].endswith('.pt'):
            self.add_error('model_path', 'The model must be a .pt file.')

        if 'data_folder' not in fcd.keys():
            self.add_error('data_folder', 'There is no such directory.')
        else:
            if not os.path.isdir(fcd['data_folder']):
                self.add_error('data_folder', 'There is no such directory.')
            elif not get_files(fcd['data_folder'], extension=constants.SUPPORTED_INPUT_IMAGE_TYPES):
                self.add_error(
                    'data_folder',
                    f'There is no data to segment. Supported file types are: '
                    f'{", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}.'
                )
        if 'model_path' in fcd and fcd['model_path']:
            ckp = torch.load(fcd['model_path'], map_location=torch.device('cpu'))
            if ckp['architecture'] in constants.THREE_D_NETWORKS:
                self.__clean_batch_size_and_input_dimensions(fcd, ['batch_size', 'input_size', 'input_depth'])
            else:
                self.__clean_batch_size_and_input_dimensions(fcd, ['batch_size', 'input_size'])

