import os

from django import forms
from django.utils.safestring import mark_safe

from aiseg import constants
from aiseg.file_handling.utils import get_files, get_sub_folders
from aiseg.models import TrainingDataset


class ModelImportForm(forms.Form):
    checkpoint_path = forms.CharField(
        max_length=500,
        label='Path to checkpoint or folder trained on the dataset',
        help_text=mark_safe(
            'Path to a single checkpoint or a folder containing multiple <b>checkpoints trained on the same (!) '
            'dataset</b>. If passing a folder, every checkpoint inside (other files will be ignored) will be loaded '
            'and imported.'),
        required=True,
        initial='',
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )
    search_in_sub_folders = forms.BooleanField(
        initial=False,
        required=False,
        label='Search also in sub directories',
        help_text='Activate to search for checkpoints also in sub directories (if you passed a folder).',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'})
    )
    only_last_of_sub_folder = forms.BooleanField(
        initial=True,
        required=False,
        label='Only last checkpoint',
        help_text='If you passed a folder, only the last checkpoint inside it or all last checkpoints inside the sub '
                  'folders will be loaded.',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'})
    )
    dataset_choice = forms.ChoiceField(
        label='Select existing datasets',
        choices=([]),
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
        help_text='Select an existing dataset or create a new one (click New Dataset).',
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.checkpoints = None
        self.dataset_name = None

        dataset_folders = get_sub_folders(constants.DATASET_FOLDER, depth=1)
        existing_dataset_names = []
        for folder in dataset_folders:
            sub_folders = get_sub_folders(folder, depth=1)
            sub_folders = [os.path.basename(sub_folder) for sub_folder in sub_folders]
            if 'images' in sub_folders and 'masks' in sub_folders:
                existing_dataset_names.append(os.path.basename(folder))

        dataset_choices = []
        datasets = TrainingDataset.objects.all().order_by('name')
        for dataset in datasets:
            if dataset.folder_name_on_device not in existing_dataset_names:
                dataset_choices.append((dataset.pk, dataset.name + ' (not on this pc)'))
            else:
                dataset_choices.append((dataset.pk, dataset.name))

        if not dataset_choices:
            dataset_choices.insert(0, constants.EMPTY_CHOICE)

        self.dataset_choice = dataset_choices
        self.fields['dataset_choice'].choices = dataset_choices
        self.fields['dataset_choice'].initial = constants.EMPTY_CHOICE[0]

    def clean(self):
        fcd = super().clean()

        if not fcd['dataset_choice']:
            self.add_error(
                'dataset_choice',
                'Please select the dataset the model was trained on or create a new dataset.'
            )

        if not os.path.isdir(fcd['checkpoint_path']) and not os.path.isfile(fcd['checkpoint_path']):
            self.add_error('checkpoint_path', 'The path does not exist!')

        elif os.path.isdir(fcd['checkpoint_path']):
            if fcd['search_in_sub_folders']:
                sub_directories = sorted(get_sub_folders(fcd['checkpoint_path']))
            else:
                sub_directories = [fcd['checkpoint_path']]

            checkpoints = []
            for sub_dir in sub_directories:
                checkpoint_files = get_files(sub_dir, extension='.pt')
                if checkpoint_files and fcd['only_last_of_sub_folder']:
                    checkpoint_files = [checkpoint_files[-1]]
                checkpoints += checkpoint_files

            if not checkpoints:
                self.add_error('checkpoint_path', 'Could not find any checkpoint!')
            else:
                self.checkpoints = checkpoints

        else:
            if not os.path.isfile(fcd['checkpoint_path']):
                self.add_error('checkpoint_path', 'The path does not exist!')

            self.checkpoints = [fcd['checkpoint_path']]

        return fcd
