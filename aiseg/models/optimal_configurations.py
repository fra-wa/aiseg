from django.db import models

from aiseg import constants


class OptimalConfiguration(models.Model):
    """Optimal training configurations. Loaded or created when calculating the optimal parameters at training view"""
    architecture = models.CharField(
        verbose_name='Architecture',
        max_length=128,
        choices=constants.ARCHITECTURE_CHOICES,
        editable=False,
    )
    backbone = models.CharField(
        verbose_name='Backbone',
        max_length=128,
        blank=True,
        editable=False,
    )
    device = models.CharField(
        verbose_name='Device',
        max_length=7,
        editable=False,
    )
    total_ram_in_mib = models.PositiveIntegerField(
        verbose_name='Total RAM on device',
        editable=False,
    )
    approximated_ram_usage = models.PositiveIntegerField(
        verbose_name='Approximated RAM usage on device',
    )

    normalization = models.CharField(
        verbose_name='Normalization Layer',
        max_length=12,
        editable=False,
    )
    pretrained = models.BooleanField(
        verbose_name='Pretrained',
        editable=False,
    )
    channels = models.PositiveSmallIntegerField(
        verbose_name='Input channels',
        editable=False,
    )
    classes = models.PositiveIntegerField(
        verbose_name='Output channels',
        editable=False,
    )
    input_height = models.PositiveIntegerField(
        verbose_name='Input size',
    )
    input_width = models.PositiveIntegerField(
        verbose_name='Input width',
    )
    max_input_size = models.PositiveIntegerField(
        verbose_name='Max data width and height',
        blank=True,
        null=True,
    )
    fixed_depth = models.BooleanField(
        verbose_name='Depth is fixed',
        editable=False,
    )
    input_depth = models.PositiveIntegerField(
        verbose_name='Input depth',
        null=True,
        default=None,
    )

    batch_size = models.PositiveIntegerField(
        verbose_name='Batch size',
    )


class InferenceConfiguration(models.Model):
    architecture = models.CharField(
        verbose_name='Architecture',
        max_length=128,
        choices=constants.ARCHITECTURE_CHOICES,
        editable=False,
    )
    backbone = models.CharField(
        verbose_name='Backbone',
        max_length=128,
        blank=True,
        null=True,
        editable=False,
    )
    normalization = models.CharField(
        verbose_name='Normalization',
        max_length=128,
        choices=constants.NORMALIZATION_CHOICES,
        editable=False,
    )

    channels = models.PositiveSmallIntegerField(
        verbose_name='Input channels',
        editable=False,
    )
    classes = models.PositiveIntegerField(
        verbose_name='Output channels',
        editable=False,
    )

    device = models.CharField(
        verbose_name='Device',
        max_length=7,
        editable=False,
    )
    device_ram = models.PositiveIntegerField(
        verbose_name='Total RAM on device',
        editable=False,
    )

    max_input_size = models.PositiveIntegerField(
        verbose_name='Max data width and height',
    )
    max_input_depth = models.PositiveIntegerField(
        verbose_name='Max data depth',
        blank=True,
        null=True,
    )

    input_height = models.PositiveIntegerField(
        verbose_name='Calculated input height',
    )
    input_width = models.PositiveIntegerField(
        verbose_name='Calculated input width',
    )
    input_depth = models.PositiveIntegerField(
        verbose_name='Calculated depth depth',
        blank=True,
        null=True,
    )
    batch_size = models.PositiveIntegerField(
        verbose_name='Calculated batch size',
    )
    approximated_ram_usage = models.PositiveIntegerField(
        verbose_name='Approximated RAM usage on device',
    )
