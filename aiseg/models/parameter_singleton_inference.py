from decimal import Decimal
from email.policy import default

import torch
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from aiseg import constants

"""
This is created once to store the users inference input over time.
"""


class InferenceParameters(models.Model):
    model_path = models.CharField(
        verbose_name='Path to checkpoint',
        max_length=256,
        blank=True,
        help_text='Pass the path to your model.',
    )
    data_folder = models.CharField(
        verbose_name='Path to the data',
        max_length=256,
        blank=True,
        help_text='Pass the path to the folder containing the images/volume.',
    )

    device_choices = [('cpu', 'CPU')]
    default_device = 'cpu'
    if torch.cuda.is_available():
        device_count = torch.cuda.device_count()
        if device_count == 1:
            device_choices.append(('cuda:0', 'GPU'))
        else:
            for i in range(device_count):
                device_choices.append((f'cuda:{i}', f'GPU:{i}'))
        default_device = 'cuda:0'

    device = models.CharField(
        verbose_name='Device',
        choices=device_choices,
        default=default_device,
        max_length=7,
        help_text='GPU is recommended if possible.',
    )
    save_color = models.BooleanField(
        verbose_name='Save output as rgb',
        default=True,
        help_text='Save the segmentation in grayscale or rgb.',
    )
    log_thresh = models.DecimalField(
        verbose_name='Logits threshold',
        decimal_places=2,
        max_digits=3,
        default=Decimal('0.5'),
        validators=[MinValueValidator(Decimal('0.0')), MaxValueValidator(Decimal('1.0'))],
        help_text='0 to 1. Set the threshold of the logits for binary classification. During training 0.5 was used. '
                  'The higher the threshold the higher the certainty of the prediction. If too high, nothing might '
                  'be segmented!.',
    )
    overlap = models.PositiveSmallIntegerField(
        verbose_name='Overlap percentage',
        default=50,
        validators=[MinValueValidator(0), MaxValueValidator(90)],
        help_text='Set the percentage of overlap (0 to 90%). Represents overlapping % in width, height and depth '
                  '(if 3d data is used). CNNs are trained/tested using 0% overlap.',
    )
    weighting_strategy = models.CharField(
        verbose_name='Weighting strategy',
        choices=constants.INFERENCE_WEIGHTING_STRATEGY_CHOICES,
        default=constants.INFERENCE_WEIGHTING_STRATEGY_CHOICES[0][0],
        max_length=255,
        help_text='Select the weighting strategy when using overlapping. Sum: each logit is equally weighted. Linear: '
                  'ones at outer region, highest in center like [1,2,3,2,1] just in 2 or 3D (weighting border logits '
                  '1/(~h/2) to that of center). Gaussian: logits are weighted using a gaussian bell (2D or 3D), '
                  'weighting border logits 1/8 to that of center (sigma h, w = h/8; sigma d = d/8).',
    )
    batch_size = models.PositiveIntegerField(
        verbose_name='Batch size',
        default=None,
        help_text='Optional. Images/Volumes per forward pass during inference. Calculated automatically (and stored to '
                  'the database) if not set. If ypu pass a value, you need to pass the batch size as well.',
        validators=[MinValueValidator(1)],
        blank=True,
        null=True,
    )
    input_size = models.PositiveIntegerField(
        verbose_name='Input size',
        validators=[MinValueValidator(32)],
        default=None,
        help_text='Optional. Height and width (h = w) in pixels/voxels. Calculated automatically (and stored to the '
                  'database) if not set. If ypu pass a value, you need to pass the batch size as well.',
        blank=True,
        null=True,
    )
    input_depth = models.PositiveIntegerField(
        verbose_name='Input depth',
        default=None,
        validators=[MinValueValidator(32)],
        help_text='Optional. For 3D networks: depth of the input block. Dimension is: (input_size x '
                  'input_size x input_depth) = (h, w, d). Calculated automatically (and stored to the database) if not '
                  'set. If ypu pass a value, you need to pass the batch size as well.',
        blank=True,
        null=True,
    )
