from django.db import migrations


def forwards(apps, schema_editor):
    analysis_model = apps.get_model('aiseg', 'ModelAnalysis')
    train_settings_model = apps.get_model('aiseg', 'TrainingParameters')

    for analysis in analysis_model.objects.all():
        analysis.lr_constant_period = analysis.total_epochs
        analysis.save()

    settings = train_settings_model.objects.last()
    if settings is not None:
        settings.lr_constant_period = settings.epochs
        settings.save()


def backwards(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('aiseg', '0007_lr_scheduling'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
