import logging
import logging.config
import logging.handlers
import os
import abc

from ..file_handling.utils import create_folder


class LoggingEvent(abc.ABC):

    @abc.abstractmethod
    def execute(self):
        pass


class LogRecordEvent(LoggingEvent):

    def __init__(self, record, logger_name):
        self.record = record
        self.logger_name = logger_name

    def execute(self):
        logger = logging.getLogger(self.logger_name)
        logger.handle(self.record)


class LoggerRemoverEvent(LoggingEvent):

    def __init__(self, logger_name):
        self.logger_name = logger_name

    def execute(self):
        logger = logging.getLogger(self.logger_name)
        logger.handlers.clear()


class LoggerCreatorEvent(LoggingEvent):

    def __init__(self, log_name, log_handler=None, log_format=None, log_level=None):
        """

        Args:
            log_name: name, will be appended by '_processID'
            log_handler: 'console' or a file path.
            log_format: Defaults to: '%(asctime)s %(levelname)s %(message)s'. see:
                https://docs.python.org/3/library/logging.html#logrecord-attributes
            log_level: defaults to logging.DEBUG
        """

        if log_handler is None:
            log_handler = 'console'
        else:
            log_base_dir = os.path.dirname(log_handler)
            if not os.path.isdir(log_base_dir):
                create_folder(log_base_dir)
            if not os.path.isdir(log_base_dir):
                raise FileNotFoundError(f'Could not create directory to store the log file: {log_base_dir}')

        if log_format is None:
            log_format = '%(asctime)s %(levelname)s %(message)s'
        if log_level is None:
            log_level = logging.DEBUG

        self.logging_format = log_format
        self.logging_level = log_level
        self.logging_handler = log_handler

        self.logger_original_name = log_name
        self.logger_name = log_name

        self.process_id = os.getpid()
        self.logger_name += f'_{self.process_id}'

    def create_handler(self):
        if self.logging_handler == 'console':
            return logging.StreamHandler()
        return logging.FileHandler(self.logging_handler, 'w', 'utf-8')

    def execute(self):
        logger = logging.getLogger(self.logger_name)
        handler = self.create_handler()
        handler.setFormatter(logging.Formatter(self.logging_format))
        logger.addHandler(handler)
        logger.setLevel(self.logging_level)
        logger.propagate = False  # Very important! Avoids infinite cycle with messages
