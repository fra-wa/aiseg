import torch

from aiseg.dl_models.utils import get_group_norm_groups, get_norm_layer_instance


class ConvReluBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super().__init__()

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = torch.nn.Conv3d if is_3d else torch.nn.Conv2d

        self.conv = conv(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            padding=padding,
            stride=stride,
            bias=False if normalization == 'bn' else True,
        )

        self.norm = get_norm_layer_instance(normalization, out_channels, not is_3d)
        self.relu = torch.nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.norm(x)
        x = self.relu(x)
        return x
    

class DoubleConvReluBlock(torch.nn.Module):
    """
    Conv Norm ReLU Conv Norm ReLU

    - keeps size
    - reduced memory consumption:
        Disable bias for convolutions directly followed by a batch norm
        torch.nn.Conv2d() has bias parameter which defaults to True (the same is true for Conv1d and Conv3d ).

        If a nn.Conv2d layer is directly followed by a nn.BatchNorm2d layer, then the bias in the convolution is not
        needed, instead use nn.Conv2d(..., bias=False, ....). Bias is not needed because in the first step BatchNorm
        subtracts the mean, which effectively cancels out the effect of bias.

        This is also applicable to 1d and 3d convolutions as long as BatchNorm (or other normalization layer) normalizes
         on the same dimension as convolution’s bias.
    """

    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super(DoubleConvReluBlock, self).__init__()
        if kernel_size % 2 == 0:
            raise ValueError('Kernel size must be odd, not even.')
        if kernel_size == 0 or kernel_size < 0:
            raise ValueError('Kernel size must be positive and odd.')

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = torch.nn.Conv3d if is_3d else torch.nn.Conv2d

        self.conv_block = torch.nn.Sequential(
            conv(
                input_channels,
                output_channels,
                kernel_size=kernel_size,
                padding=padding,
                stride=stride,
                bias=False if normalization == 'bn' else True,
            ),
            get_norm_layer_instance(normalization, output_channels, not is_3d),
            torch.nn.ReLU(inplace=True),
            conv(
                output_channels,
                output_channels,
                kernel_size=kernel_size,
                padding=padding,
                stride=stride,
                bias=False if normalization == 'bn' else True,
            ),
            get_norm_layer_instance(normalization, output_channels, not is_3d),
            torch.nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv_block(x)
        return x
