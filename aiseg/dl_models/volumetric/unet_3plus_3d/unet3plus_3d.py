"""
UNet 3+ paper: https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
from .unet3plus import UNet3Plus


class UNet3Plus3D(UNet3Plus):
    def __init__(self, image_channels, num_classes, normalization='gn', **kwargs):
        super(UNet3Plus3D, self).__init__(image_channels, num_classes, is_3d=True, normalization=normalization)
