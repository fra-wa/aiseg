import cv2
import logging
import numpy as np
import os
import torch
import warnings

from matplotlib import pyplot as plt

from ..file_handling.utils import create_folder
from ..training.transformations import get_gray_image_transformation


class Filter:
    def __init__(self, related_layer, filter_nr, filter_map, global_min, global_max):
        """

        Args:
            related_layer:
            filter_nr: of the related layer, one layer contains n filters
            filter_map: the actual feature map
            global_min: minimum weight of all filters of current group, needed for normalization
            global_max: maximum weight of all filters of current group, needed for normalization
        """
        if not torch.is_tensor(filter_map):
            raise RuntimeError('Filter map is not a torch tensor!')
        self.layer_name = related_layer.replace('.', '_')
        self.out_name = f'filter_nr_{filter_nr}.png'
        self.torch_filter = filter_map
        self.global_min = global_min
        self.global_max = global_max
        if self.global_min < 0:
            self.offset = -1 * self.global_min
        else:
            self.offset = -self.global_min

    def save(self, out_folder):
        # converts filter to range: 0, 255 based on all weights of current con convolution
        np_filter = self.torch_filter.cpu().detach().numpy()

        max_weight = self.global_max + self.offset
        multiplier = 255 / max_weight
        # normalize
        np_filter_image = (np_filter + self.offset) * multiplier
        np_filter_image = np.round(np_filter_image, decimals=0)
        if np.max(np_filter_image) > 255 or np.min(np_filter_image) < 0:
            raise RuntimeError('mistake')
        np_filter_image = np_filter_image.astype(np.uint8)
        np_filter_image = np.moveaxis(np_filter_image, 0, -1)

        out_folder = os.path.join(out_folder, self.layer_name)
        create_folder(out_folder)
        out_path = os.path.join(out_folder, self.out_name)
        cv2.imwrite(out_path, np_filter_image)


class BaseModel(torch.nn.Module):
    """
    Class which ships some helpers to all dl_models
    """

    def __init__(self, in_channels, num_classes):
        super(BaseModel, self).__init__()
        # mean and standard deviation of the training dataset. This is stored at the checkpoint and is set during
        # runtime while loading from checkpoint.
        self.dataset_mean = None
        self.dataset_std = None
        self.in_channels = in_channels
        if num_classes == 2:
            # binary!
            out_channels = 1
        else:
            out_channels = num_classes
        self.out_channels = out_channels

    @property
    def first_layer_padding(self):
        name, conv = self.get_first_layer_information()
        return conv.padding

    def forward(self, **kwargs):
        raise NotImplementedError('implement at your model')

    def get_parameter_count(self):
        """Counts all trainable parameters of this model
        """
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

    def get_sorted_convolutions_and_names(self, layer=None, layer_name=None):
        """
        iterates through a model or layer and returns all convolutional layers and their names

        Args:
            layer:
            layer_name:

        Returns: list of all convolutions and names of this model like: [(layer_name, convolution)]

        """
        if layer is None:
            layer = self

        convolutions = []
        for name, child in layer.named_children():
            if isinstance(child, torch.nn.Conv2d) or isinstance(child, torch.nn.Conv3d):
                if layer_name is not None:
                    name = f'{layer_name}_{name}'
                convolutions.append((name, child))
            else:
                if layer_name is not None:
                    name = f'{layer_name}_{name}'
                convolutions += (self.get_sorted_convolutions_and_names(child, name))

        return convolutions

    def save_feature_maps(self, out_folder):
        """
        This function iterates over the whole model and returns saves all filter maps for each sub layer

        Returns: all filters layer by layer as ordered dict

        """
        convolutions = self.get_sorted_convolutions_and_names()
        for layer_idx, name_conv in enumerate(convolutions):
            name, conv = name_conv

            if not isinstance(conv, torch.nn.Conv2d):
                raise RuntimeError

            num_filters = conv.out_channels
            # out channels might be something like: 128x128x3x3 --> saving only first eight of 16384 in that case
            global_min = torch.min(conv.weight).item()
            global_max = torch.max(conv.weight).item()
            for i in range(num_filters):
                if i == 8:
                    # save only first 8 filters
                    break
                feature = Filter(
                    related_layer=name.replace('.', '_'),
                    filter_nr=i,
                    filter_map=conv.weight[i, 0, :, :],
                    global_min=global_min,
                    global_max=global_max,
                )
                feature.save(out_folder)

    def get_convolutional_outputs(self, img_tensor):
        convolutions = self.get_sorted_convolutions_and_names()
        results = [(convolutions[0][0], convolutions[0][1](img_tensor))]
        convolutions.pop(0)

        for name_conv in convolutions:
            name, conv = name_conv
            # Todo: Resnet has residual connections --> handle downsample..
            try:
                result = conv(results[-1][1])
                results.append((name, result))
            except RuntimeError:
                logging.info(f'Stuck at layer: {name}! Could not go deeper due to special architectural behaviours.')
                return results
        return results

    def visualize_filters_by_image(self, image_path, out_folder):
        """
        Only suitable for gray and 2D! (rgb will be converted to gray)

        This will visualize each filter of each convolutional layer by applying it to the image.
        The image is passed forward through each layer step by step so that one really sees, what happens.

        Args:
            image_path: path to input image
            out_folder: out folder of all output images

        Returns:

        """

        img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
        transformation = get_gray_image_transformation()
        transformed_img = transformation(img)
        transformed_img = torch.unsqueeze(transformed_img, dim=0)

        results = self.get_convolutional_outputs(transformed_img)

        for idx, name_and_out in enumerate(results):

            name, out_img = name_and_out
            out_data = out_img[0, :, :, :].data

            for sub_idx, filtered in enumerate(out_data):
                if sub_idx == 64:
                    # only 8x8 blocks from each layer
                    break
                plt.subplot(8, 8, sub_idx + 1)
                plt.imshow(filtered, cmap='gray')
                plt.axis('off')

            name = name.replace('.', '_')
            file_name = f'{name}.png'
            out_file = os.path.join(out_folder, file_name)
            plt.savefig(out_file)
            plt.close()

    def get_first_layer_information(self):
        # works currently always!
        name, conv_layer = self.get_sorted_convolutions_and_names()[0]
        return name, conv_layer

    def get_last_layer_information(self):
        # does not always work!
        warnings.warn('Getting last layer information is not stable.')
        # Todo: check all model implementations - set the last layer to the end of the class __init__
        name, conv_layer = self.get_sorted_convolutions_and_names()[-1]
        return name, conv_layer
