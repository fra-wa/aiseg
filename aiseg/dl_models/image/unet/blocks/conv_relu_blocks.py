from torch import nn

from aiseg.dl_models.utils import get_norm_layer


class DoubleConvReluBlock(nn.Module):
    """
    Conv Norm ReLU Conv Norm ReLU

    - keeps size
    - optimized: https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#disable-bias-for-convolutions-directly-followed-by-a-batch-norm
    """

    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super(DoubleConvReluBlock, self).__init__()
        if kernel_size % 2 == 0:
            raise ValueError('Kernel size must be odd, not even.')
        if kernel_size == 0 or kernel_size < 0:
            raise ValueError('Kernel size must be positive and odd.')

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = nn.Conv3d if is_3d else nn.Conv2d
        norm_layer = get_norm_layer(normalization, is_2d=not is_3d)
        bias = False if normalization == 'bn' else True

        self.conv_block = nn.Sequential(
            conv(input_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride, bias=bias),
            norm_layer(output_channels),
            nn.ReLU(inplace=True),
            conv(output_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride, bias=bias),
            norm_layer(output_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv_block(x)
        return x


class ConvReluBlock(nn.Module):
    """
    Conv Norm ReLU
    - keeps size
    - optimized: https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#disable-bias-for-convolutions-directly-followed-by-a-batch-norm
    """
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super().__init__()

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = nn.Conv3d if is_3d else nn.Conv2d
        norm_layer = get_norm_layer(normalization, is_2d=not is_3d)
        self.norm = norm_layer(out_channels)
        self.conv = conv(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            padding=padding,
            stride=stride,
            bias=False if normalization == 'bn' else True,
        )
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.norm(x)
        x = self.relu(x)
        return x


class ResidualDoubleConvBlock(nn.Module):
    """
    out = Conv ReLU (x)
    residual = out
    residual_block = Conv ReLU Conv ReLU (out)
    out = residual + residual_block
    out = ReLU(out)

    - keeps size
    - optimized: https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#disable-bias-for-convolutions-directly-followed-by-a-batch-norm
    """

    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super(ResidualDoubleConvBlock, self).__init__()
        if kernel_size % 2 == 0:
            raise ValueError('Kernel size must be odd, not even.')
        if kernel_size == 0 or kernel_size < 0:
            raise ValueError('Kernel size must be positive and odd.')

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = nn.Conv3d if is_3d else nn.Conv2d
        norm_layer = get_norm_layer(normalization, is_2d=not is_3d)
        bias = False if normalization == 'bn' else True

        self.norm = norm_layer(output_channels)
        self.conv_1 = nn.Sequential(
            conv(input_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride, bias=bias),
            norm_layer(output_channels),
            nn.ReLU(inplace=True),
        )
        self.conv_2 = nn.Sequential(
            conv(output_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride, bias=bias),
            norm_layer(output_channels),
            nn.ReLU(inplace=True)
        )

        self.conv_3 = conv(
            output_channels,
            output_channels,
            kernel_size=kernel_size,
            padding=padding,
            stride=stride,
            bias=bias,
        )
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv_1(x)
        residual = x

        x = self.conv_2(x)
        x = self.conv_3(x)
        x = self.norm(x)

        x += residual
        return self.relu(x)
