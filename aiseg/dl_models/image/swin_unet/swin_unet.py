"""Adapted from: https://github.com/HuCaoFighting/Swin-Unet"""
import warnings

from aiseg import constants
from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.image.swin_unet.swin_transformer import SwinTransformerSys
from aiseg.dl_models.utils import get_norm_layer


class SwinUnet(BaseModel):
    def __init__(self, image_channels, num_classes, in_size, **kwargs):
        super(SwinUnet, self).__init__(image_channels, num_classes)

        norm_layer = get_norm_layer(constants.LAYER_NORM)
        if num_classes == 2:
            num_classes = 1

        if in_size % 7 == 0:
            # original - only works for 224 or 384
            window_size = 7
        else:
            for choice in constants.IN_SIZE_CHOICES_2D:
                if choice[0] % 8 != 0:
                    # log and print if this fix does not work
                    warnings.warn(f'Attention: SwinUnet cannot process images of size: {choice[0]}')
                    print(f'Attention: SwinUnet cannot process images of size: {choice[0]}')
            window_size = 8
            if in_size % 8 != 0:
                raise ValueError(f'SwinUnet cannot process an in_size of: {in_size}')

        self.swin_unet = SwinTransformerSys(
            img_size=in_size,
            patch_size=4,
            in_chans=image_channels,
            num_classes=num_classes,
            embed_dim=96,
            depths=[2, 2, 6, 2],
            num_heads=[3, 6, 12, 24],
            window_size=window_size,
            mlp_ratio=4.,
            qkv_bias=True,
            qk_scale=None,
            drop_rate=0.0,
            drop_path_rate=0.1,
            ape=False,
            patch_norm=True,
            norm_layer=norm_layer,
        )

    def forward(self, x):
        x = self.swin_unet(x)
        return x
