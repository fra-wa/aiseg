import torch


class ConvLSTMCell(torch.nn.Module):
    """
    Basic CLSTM cell.
    """

    def __init__(self, in_channels, hidden_channels, kernel_size, bias=True, device='cuda'):
        """

        Args:
            in_channels (int): Number of channels of input tensor.
            hidden_channels (int): Number of channels of hidden state.
            kernel_size (tuple): Size of the convolutional kernel.
            bias (bool): Whether or not to add the bias.
        """
        super(ConvLSTMCell, self).__init__()

        self.device = torch.device(device)
        self.input_dim = in_channels
        self.hidden_dim = hidden_channels

        self.kernel_size = kernel_size
        self.padding = int(kernel_size[0] / 2), int(kernel_size[1] / 2)
        self.bias = bias

        self.conv = torch.nn.Conv2d(
            in_channels=self.input_dim + self.hidden_dim,  # because: concatenate(input, hidden)
            out_channels=4 * self.hidden_dim,  # times 4 because 4 gates
            kernel_size=self.kernel_size,
            padding=self.padding,
            bias=self.bias,
        )

    def forward(self, input_tensor, previous_state):
        """
        See: https://colah.github.io/posts/2015-08-Understanding-LSTMs/
        to follow code

        Args:
            input_tensor: x(t) [b, C, H, W]
            previous_state: list containing [previous_hidden, previous_cell_state]  ->  [h(t-1), C(t-1)]

        Returns: h_cur, c_cur  -> h(t) [b, C, H, W], C(t)

        """
        h_previous, c_previous = previous_state

        x_t_h_prev = torch.cat((input_tensor, h_previous), dim=1)

        gates = self.conv(x_t_h_prev)

        cell_f, cell_i, candidates, cell_o = torch.split(gates, self.hidden_dim, dim=1)
        f_t = torch.sigmoid(cell_f)  # forget
        i_t = torch.sigmoid(cell_i)  # input
        c_g = torch.tanh(candidates)  # candidate gate
        o_t = torch.sigmoid(cell_o)  # output

        c_current = f_t * c_previous + i_t * c_g
        h_current = o_t * torch.tanh(c_current)

        return h_current, c_current

    def init_hidden(self, batch_size, height, width):
        if self.device == torch.device('cuda'):
            return (torch.zeros(batch_size, self.hidden_dim, height, width).cuda(),
                    torch.zeros(batch_size, self.hidden_dim, height, width).cuda())
        else:
            return (torch.zeros(batch_size, self.hidden_dim, height, width).cpu(),
                    torch.zeros(batch_size, self.hidden_dim, height, width).cpu())


class ConvGRUCell(torch.nn.Module):
    """
    Basic CGRU cell.
    Less weights than LSTM and equal performance.
    """

    def __init__(self, in_channels, hidden_channels, kernel_size, bias, device='cuda'):

        super(ConvGRUCell, self).__init__()

        self.device = torch.device(device)

        self.input_dim = in_channels
        self.hidden_dim = hidden_channels

        self.kernel_size = kernel_size
        self.padding = kernel_size[0] // 2, kernel_size[1] // 2
        self.bias = bias
        self.update_gate = torch.nn.Conv2d(
            in_channels=self.input_dim + self.hidden_dim,
            out_channels=self.hidden_dim,
            kernel_size=self.kernel_size,
            padding=self.padding,
            bias=self.bias,
        )
        self.reset_gate = torch.nn.Conv2d(
            in_channels=self.input_dim + self.hidden_dim,
            out_channels=self.hidden_dim,
            kernel_size=self.kernel_size,
            padding=self.padding,
            bias=self.bias,
        )

        self.out_gate = torch.nn.Conv2d(
            in_channels=self.input_dim + self.hidden_dim,
            out_channels=self.hidden_dim,
            kernel_size=self.kernel_size,
            padding=self.padding,
            bias=self.bias,
        )

    def forward(self, input_tensor, previous_state):
        """
        See: https://towardsdatascience.com/understanding-gru-networks-2ef37df6c9be
         to follow code

        Args:
            input_tensor: also named x(t) [b, C, H, W]
            previous_state: h(t-1) [b, C, H, W]
        """
        x_t_h_prev = torch.cat((input_tensor, previous_state), dim=1)

        update_gate = torch.sigmoid(self.update_gate(x_t_h_prev))  # z(t)
        reset_gate = torch.sigmoid(self.reset_gate(x_t_h_prev))  # r(t)

        # h'(t)
        current_memory = torch.tanh(self.out_gate(torch.cat((input_tensor, previous_state * reset_gate), dim=1)))
        h_current = update_gate * previous_state + current_memory * (1 - update_gate)

        return h_current

    def init_hidden(self, batch_size, h, w):
        if self.device == torch.device('cuda'):
            return torch.zeros(batch_size, self.hidden_dim, h, w).cuda()
        else:
            return torch.zeros(batch_size, self.hidden_dim, h, w).cpu()
