import torch

from aiseg.dl_models.base_model import BaseModel
from aiseg.dl_models.image.unet.blocks.decoders import CRDecoder
from .blocks.conv_relu_blocks import DoubleConvReluBlock
from .blocks.encoders import DoubleCREncoder


class UNet(BaseModel):
    def __init__(self, image_channels, num_classes, normalization='gn', **kwargs):
        super(UNet, self).__init__(image_channels, num_classes)

        if num_classes == 2:
            # binary!
            num_classes = 1

        self.encoder_1 = DoubleCREncoder(image_channels, 64, normalization=normalization)
        self.encoder_2 = DoubleCREncoder(64, 128, normalization=normalization)
        self.encoder_3 = DoubleCREncoder(128, 256, normalization=normalization)
        self.encoder_4 = DoubleCREncoder(256, 512, normalization=normalization)

        self.center = DoubleConvReluBlock(512, 1024, normalization=normalization)

        self.decoder_1 = CRDecoder(1024 + 512, 1024, 512, normalization=normalization)
        self.decoder_2 = CRDecoder(512 + 256, 512, 256, normalization=normalization)
        self.decoder_3 = CRDecoder(256 + 128, 256, 128, normalization=normalization)
        self.decoder_4 = CRDecoder(128 + 64, 128, 64, normalization=normalization)

        self.final = torch.nn.Conv2d(64, num_classes, kernel_size=1, padding=0, stride=1)

    def forward(self, x):
        enc_1 = self.encoder_1(x)
        enc_2 = self.encoder_2(enc_1)
        enc_3 = self.encoder_3(enc_2)
        enc_4 = self.encoder_4(enc_3)

        center = self.center(enc_4)

        x = self.decoder_1(torch.cat((center, enc_4), dim=1))
        x = self.decoder_2(torch.cat((x, enc_3), dim=1))
        x = self.decoder_3(torch.cat((x, enc_2), dim=1))
        x = self.decoder_4(torch.cat((x, enc_1), dim=1))

        x = self.final(x)
        return x
