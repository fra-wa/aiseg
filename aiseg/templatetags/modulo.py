from django.template.defaulttags import register


@register.filter
def modulo(number, value):
    return number % value
