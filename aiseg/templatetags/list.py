from django.template.defaulttags import register


@register.filter
def is_in_string_list(item_to_search, string_list):
    search_list = string_list.split(';')
    if item_to_search in search_list:
        return True
    return False
