from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import SwinUnet


class SwinUnetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('SwinUnet', SwinUnet, **{'in_size': 256})
