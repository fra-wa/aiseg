import cv2
import numpy as np
import os

from unittest import TestCase
from unittest import mock

from aiseg.data_augmentation.geometric_transformations import rotate_by_angle, resize
from aiseg.data_augmentation.geometric_volume_transformations import rotate_z
from aiseg.data_augmentation.online_augmentation_pipelines import ImageAugmentFunc
from aiseg.data_augmentation.online_augmentation_pipelines import ImageAugmentationPipeline
from aiseg.data_augmentation.online_augmentation_pipelines import VolumeAugmentFunc
from aiseg.data_augmentation.online_augmentation_pipelines import VolumeAugmentationPipeline
from aiseg.file_handling.utils import get_files, create_folder, remove_dir


def mocked_randint_second_val(val1, val2):
    a = 0
    return val2


def save_volume_slices(volume, folder):
    depth, height, width = volume.shape[:3]
    create_folder(folder)

    for z in range(depth):
        img = volume[z]
        file_nr = str(z).zfill(3)
        file_name = 'slice_' + file_nr + '.png'
        file_path = os.path.join(folder, file_name)
        cv2.imwrite(file_path, img)


class AugmentFunctionTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        color_file = os.path.join(files_path, 'augmentation', 'color_img', 'rgb.jpg')
        color_mask_file = os.path.join(files_path, 'augmentation', 'color_mask', 'rgb.png')
        self.color_img = cv2.imread(color_file, -1)
        self.gray_img = cv2.imread(color_mask_file, cv2.IMREAD_GRAYSCALE)

        volume_dir = os.path.join(files_path, 'data_3d', 'images', 'volume_1')
        self.volume = self.create_volume_from_files(get_files(volume_dir))

    @staticmethod
    def create_volume_from_files(files):
        volume = np.zeros((10, 100, 100), dtype=np.uint8)

        for z, img in enumerate(files):
            if z == 10:
                break
            volume[z] = cv2.imread(img, cv2.IMREAD_GRAYSCALE)[:100, :100]
        return volume

    def test_image_augment_function(self):
        kwargs = {'max_rot_angle': 90}
        func = ImageAugmentFunc(func=rotate_by_angle, channels=[1, 3], probability=1)
        img = self.color_img[:100, :200]
        mask = self.gray_img[:100, :200]
        with mock.patch('random.randint', mocked_randint_second_val):
            augmented_img, augmented_mask, augmentation_was_executed = func(img, mask, **kwargs)
            self.assertEqual(augmented_img.shape[0], 200)
            self.assertEqual(augmented_img.shape[1], 100)

        func = ImageAugmentFunc(func=rotate_by_angle, channels=3, probability=1)
        img = self.color_img[:100, :200]
        mask = self.gray_img[:100, :200]
        with mock.patch('random.randint', mocked_randint_second_val):
            augmented_img, augmented_mask, augmentation_was_executed = func(img, mask, **kwargs)
            self.assertEqual(augmented_img.shape[0], 200)
            self.assertEqual(augmented_img.shape[1], 100)

    def test_image_resizing_function(self):
        func = ImageAugmentFunc(func=resize, channels=3, probability=1, **{'expected_input_size': 256})
        img = self.color_img[:300, :300]
        mask = self.gray_img[:300, :300]
        with mock.patch('random.randint', mocked_randint_second_val):
            augmented_img, augmented_mask, augmentation_was_executed = func(img, mask)
            self.assertEqual(augmented_img.shape[0], 256)
            self.assertEqual(augmented_img.shape[1], 256)

    def test_volume_augment_function(self):
        kwargs = {'max_rot_angle': 90}
        func = VolumeAugmentFunc(func=rotate_z, channels=1, probability=1)
        volume = self.volume[:, :50, :100]
        mask = np.zeros_like(volume)
        with mock.patch('random.randint', mocked_randint_second_val):
            augmented_img, augmented_mask, augmentation_was_executed = func(volume, mask, **kwargs)
            self.assertEqual(augmented_img.shape[0], 10)
            self.assertEqual(augmented_img.shape[1], 100)
            self.assertEqual(augmented_img.shape[2], 50)


class AugmentPipelinesTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        color_file = os.path.join(files_path, 'augmentation', 'color_img', 'rgb.jpg')
        color_mask_file = os.path.join(files_path, 'augmentation', 'color_mask', 'rgb.png')
        self.color_img = cv2.imread(color_file, -1)
        self.gray_img = cv2.imread(color_mask_file, cv2.IMREAD_GRAYSCALE)

        volume_dir = os.path.join(files_path, 'data_3d', 'images', 'volume_1')
        self.volume = self.create_volume_from_files(get_files(volume_dir))
        volume_dir = os.path.join(files_path, 'data_3d', 'masks', 'volume_1')
        self.volume_mask = self.create_volume_from_files(get_files(volume_dir))

        self.temp_dir = os.path.join(files_path, 'tmp')
        create_folder(self.temp_dir)

    def tearDown(self) -> None:
        if os.path.isdir(self.temp_dir):
            remove_dir(self.temp_dir)

    @staticmethod
    def create_volume_from_files(files):
        depth = 100
        volume = np.zeros((depth, 100, 100), dtype=np.uint8)

        for z, img in enumerate(files):
            if z == depth:
                break
            volume[z] = cv2.imread(img, cv2.IMREAD_GRAYSCALE)[:100, :100]
        return volume

    def test_image_augment_pipeline(self):
        kwargs = {
            'global_strength': 1,
            'expected_input_size': 100,

            'max_rot_angle': 30,
            'resizing_scale': (0.8, 0.8),
            'squeeze_scale': (0.8, 0.8),
            'vertical_flip_allowed': True,
            'tilting_start_end_factor': (1 / 5, 4 / 5),
        }
        pixel_args_to_test = [
            'brightness_prob',
            'contrast_prob',
            'gaussian_filter_prob',
            'gaussian_noise_prob',
            'random_erasing_prob',
            'channel_shuffle_prob',
            'color_to_hsv_prob',
            'iso_noise_prob',
            'random_fog_prob',
            'random_rain_prob',
            'random_shadow_prob',
            'random_snow_prob',
            'random_sun_flair_prob',
            'rgb_shift_prob',
            'solarize_prob',
            'to_gray_prob',
            'to_sepia_prob',
        ]
        geo_args_to_test = [
            'elastic_distortion_prob',
            'flip_prob',
            'grid_distortion_prob',
            'optical_distortion_prob',
            'random_crop_prob',
            'resize_prob',
            'rotate_by_angle_prob',
            'tilt_prob',
        ]
        input_image = self.color_img[:100, :100]
        target_image = self.gray_img[:100, :100]

        with mock.patch('random.randint', mocked_randint_second_val):
            for test_argument in pixel_args_to_test:
                kwargs[test_argument] = 1
                pipeline = ImageAugmentationPipeline(**kwargs)
                test_img = np.copy(input_image)
                test_mask = np.copy(target_image)
                img, mask = pipeline(test_img, test_mask)
                try:
                    self.assertFalse(np.array_equal(img, input_image))
                    self.assertTrue(np.array_equal(mask, target_image))
                except AssertionError as e:
                    raise AssertionError(f'Augment pipeline failed at argument: {test_argument}. Error was:\n{e}')
                kwargs[test_argument] = 0

            for test_argument in geo_args_to_test:
                kwargs[test_argument] = 1
                pipeline = ImageAugmentationPipeline(**kwargs)
                img, mask = pipeline(input_image, target_image)
                try:
                    self.assertFalse(np.array_equal(img, input_image))
                    self.assertFalse(np.array_equal(mask, target_image))
                except AssertionError as e:
                    raise AssertionError(f'Augment pipeline failed at argument: {test_argument}. Error was:\n{e}')
                kwargs[test_argument] = 0

    def test_volume_augment_pipeline(self):
        kwargs = {
            'global_strength': 1,
            'expected_input_size': 100,
            'expected_input_depth': 50,
        }

        pixel_args_to_test = [
            'add_blur_3d_prob',
            'add_noise_3d_prob',
            'brightness_3d_prob',
            'contrast_3d_prob',
            'random_erasing_3d_prob',
            'sharpen_3d_with_blur_prob',
        ]
        geo_args_to_test = [
            'flip_prob',
            'random_crop_3d_prob',  # expects the volume to be at least 32³ voxels
            'random_resize_3d_prob',
            'rotate_3d_prob',
            'squeeze_prob',
            'tilt_prob',
        ]

        with mock.patch('random.randint', mocked_randint_second_val):
            for test_argument in pixel_args_to_test:
                kwargs[test_argument] = 1
                pipeline = VolumeAugmentationPipeline(**kwargs)
                in_vol = np.copy(self.volume[:50, :, :])
                target_vol = np.copy(self.volume[:50, :, :])
                inputs, targets = pipeline(in_vol, target_vol)
                try:
                    self.assertFalse(np.array_equal(inputs, self.volume[:50, :, :]))
                    self.assertTrue(np.array_equal(targets, self.volume[:50, :, :]))
                    kwargs[test_argument] = 0
                except AssertionError as e:
                    raise AssertionError(f'Augment pipeline failed at argument: {test_argument}. Error was:\n{e}')

            for test_argument in geo_args_to_test:
                kwargs[test_argument] = 1
                pipeline = VolumeAugmentationPipeline(**kwargs)
                in_vol = np.copy(self.volume[:20, :, :])
                target_vol = np.copy(self.volume[:20, :, :])
                inputs, targets = pipeline(in_vol, target_vol)
                try:
                    self.assertFalse(np.array_equal(inputs, self.volume[:20, :, :]))
                    self.assertFalse(np.array_equal(targets, self.volume[:20, :, :]))
                except AssertionError as e:
                    raise AssertionError(f'Augment pipeline failed at argument: {test_argument}. Error was:\n{e}')
                kwargs[test_argument] = 0

    def test_online_aug_resizing_2d_3d(self):
        # 2d
        kwargs = {
            'global_strength': 1,
            'expected_input_size': 400,
            'resize_prob': 1,
            'resizing_scale': (0.8, 0.8),
        }

        input_image = self.color_img[:400, :400]
        target_image = self.gray_img[:400, :400]

        with mock.patch('random.randint', mocked_randint_second_val):
            pipeline = ImageAugmentationPipeline(**kwargs)
            img, mask = pipeline(input_image, target_image)
            self.assertFalse(np.array_equal(img, input_image))
            self.assertFalse(np.array_equal(mask, target_image))
            file_name = f'online_aug_resizing_0_8.png'
            file_path = os.path.join(self.temp_dir, file_name)
            cv2.imwrite(file_path, img)
            self.assertTrue(os.path.isfile(file_path))  # set debug point for manual check.

        # 2d
        kwargs = {
            'global_strength': 1,
            'expected_input_size': 400,
            'resize_prob': 1,
            'resizing_scale': (0.8, 0.8),
        }

        input_image = self.color_img[:400, :100]
        target_image = self.gray_img[:400, :100]

        with mock.patch('random.randint', mocked_randint_second_val):
            pipeline = ImageAugmentationPipeline(**kwargs)
            img, mask = pipeline(input_image, target_image)
            self.assertFalse(np.array_equal(img, input_image))
            self.assertFalse(np.array_equal(mask, target_image))
            file_name = f'online_aug_resizing_0_8.png'
            file_path = os.path.join(self.temp_dir, file_name)
            cv2.imwrite(file_path, img)
            self.assertTrue(os.path.isfile(file_path))  # set debug point for manual check.

        # 3d
        kwargs = {
            'global_strength': 1,
            'expected_input_size': 100,
            'expected_input_depth': 100,
            'random_resize_3d_prob': 1,
            'resizing_scale': (0.8, 0.8),
        }

        with mock.patch('random.randint', mocked_randint_second_val):
            pipeline = VolumeAugmentationPipeline(**kwargs)
            img, mask = pipeline(self.volume, self.volume_mask)
            self.assertFalse(np.array_equal(img, input_image))
            self.assertFalse(np.array_equal(mask, target_image))
            vol_folder_name = f'online_aug_resizing_0_8'
            vol_folder = os.path.join(self.temp_dir, vol_folder_name)
            create_folder(vol_folder)
            save_volume_slices(img, vol_folder)
            self.assertTrue(len(get_files(vol_folder)), 100)  # set debug point for manual check.

        with mock.patch('random.randint', mocked_randint_second_val):
            pipeline = VolumeAugmentationPipeline(**kwargs)
            img, mask = pipeline(self.volume[:50, :75, :25], self.volume_mask[:50, :75, :25])
            self.assertFalse(np.array_equal(img, input_image))
            self.assertFalse(np.array_equal(mask, target_image))
            vol_folder_name = f'online_aug_resizing_0_8'
            vol_folder = os.path.join(self.temp_dir, vol_folder_name)
            create_folder(vol_folder)
            save_volume_slices(img, vol_folder)
            self.assertTrue(len(get_files(vol_folder)), 100)  # set debug point for manual check.
