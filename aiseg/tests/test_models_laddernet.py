from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import LadderNet


class LadderNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('LadderNet', LadderNet)
