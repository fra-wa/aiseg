from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import FCN8
from aiseg.dl_models import FCN16
from aiseg.dl_models import FCN32


class FCNTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('FCN8', FCN8)
        self.start_test('FCN16', FCN16)
        self.start_test('FCN32', FCN32)
