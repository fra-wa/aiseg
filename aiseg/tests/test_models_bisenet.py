import warnings

from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import BiSeNet
from aiseg.dl_models import BiSeNetV2


class BiSeNetTests(ModelBaseTests2D):
    def test_forward(self):
        with warnings.catch_warnings(record=True):
            self.start_test('BiSeNet', BiSeNet)


class BiSeNetV2Tests(ModelBaseTests2D):
    def test_forward(self):
        with warnings.catch_warnings(record=True):
            self.start_test('BiSeNetV2', BiSeNetV2)
