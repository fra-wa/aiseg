import cv2
import os

from unittest import TestCase

import numpy as np

from aiseg.data_augmentation.geometric_transformations import flip_x
from aiseg.data_augmentation.geometric_transformations import flip_y
from aiseg.data_augmentation.geometric_transformations import flip_xy
from aiseg.data_augmentation.geometric_transformations import elastic_distortion
from aiseg.data_augmentation.geometric_transformations import grid_distortion
from aiseg.data_augmentation.geometric_transformations import optical_distortion
from aiseg.data_augmentation.geometric_transformations import random_crop
from aiseg.data_augmentation.geometric_transformations import random_grid_shuffle
from aiseg.data_augmentation.geometric_transformations import resize
from aiseg.data_augmentation.geometric_transformations import rotate_by_angle
from aiseg.data_augmentation.geometric_transformations import squeeze_height
from aiseg.data_augmentation.geometric_transformations import squeeze_width
from aiseg.data_augmentation.geometric_transformations import tilt_backwards
from aiseg.data_augmentation.geometric_transformations import tilt_forward
from aiseg.data_augmentation.geometric_transformations import tilt_left
from aiseg.data_augmentation.geometric_transformations import tilt_right
from aiseg.file_handling.utils import create_folder


class GeometricManipulationTests(TestCase):
    def setUp(self) -> None:
        self.save_files = False

        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        augmentation_folder = os.path.join(files_path, 'augmentation')
        self.result_folder = os.path.join(augmentation_folder, 'results')

        self.file_path = os.path.join(augmentation_folder, 'small.png')
        self.color_mask = os.path.join(augmentation_folder, 'color_mask.png')
        self.pattern = cv2.imread(self.file_path)
        self.mask = cv2.imread(self.color_mask)
        self.mask_gray = cv2.imread(self.file_path, cv2.IMREAD_GRAYSCALE)
        self.mask_gray[self.mask_gray >= 200] = 255
        self.mask_gray[self.mask_gray < 200] = 10

    def test_rotate_by_angle(self):
        rotation_folder = os.path.join(self.result_folder, 'rotation')
        if self.save_files:
            create_folder(rotation_folder)

        for i in range(360):
            img, mask = rotate_by_angle(self.pattern, self.mask_gray, i)
            self.assertEqual(img.shape[0], mask.shape[0])
            self.assertEqual(img.shape[1], mask.shape[1])
            self.assertEqual(np.unique(mask).shape[0], 2)
            try:
                self.assertNotIn(0, np.unique(mask))
            except AssertionError:
                self.fail(f'Failed at rotation, angle={i}! 0 was found in mask')

            if i % 13 == 0 and self.save_files:
                file_name = 'angle_' + str(i).zfill(3) + '_img.png'
                cv2.imwrite(os.path.join(rotation_folder, file_name), img)
                file_name = 'angle_' + str(i).zfill(3) + '_mask.png'
                cv2.imwrite(os.path.join(rotation_folder, file_name), mask)

            unique_mask_colors = np.unique(self.mask).shape[0]
            img, mask = rotate_by_angle(self.pattern, self.mask, i)
            self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_squeeze(self):
        squeeze_folder = os.path.join(self.result_folder, 'squeeze')
        if self.save_files:
            create_folder(squeeze_folder)

        for i in range(1, 10, 3):
            scale = i / 10
            img, mask = squeeze_height(self.pattern, self.mask_gray, scale)
            self.assertEqual(np.unique(mask).shape[0], 2)
            if self.save_files:
                scale_text = f'{scale:.1f}'.replace('.', '_')
                file_name = f'squeezed_h_{scale_text}.png'
                cv2.imwrite(os.path.join(squeeze_folder, file_name), img)

            img, mask = squeeze_width(self.pattern, self.mask_gray, scale)
            self.assertEqual(np.unique(mask).shape[0], 2)
            if self.save_files:
                scale_text = f'{scale:.1f}'.replace('.', '_')
                file_name = f'squeezed_w_{scale_text}.png'
                cv2.imwrite(os.path.join(squeeze_folder, file_name), img)

            unique_mask_colors = np.unique(self.mask).shape[0]
            img, mask = squeeze_width(self.pattern, self.mask, scale)
            self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

            unique_mask_colors = np.unique(self.mask).shape[0]
            img, mask = squeeze_height(self.pattern, self.mask, scale)
            self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_resize(self):
        resize_folder = os.path.join(self.result_folder, 'resizing')
        if self.save_files:
            create_folder(resize_folder)

        for i in range(5, 20, 5):
            scale = i / 10
            img, mask = resize(self.pattern, self.mask_gray, scale)
            self.assertEqual(np.unique(mask).shape[0], 2)
            if self.save_files:
                scale_text = f'{scale:.1f}'.replace('.', '_')
                file_name = f'resized_by_{scale_text}.png'
                cv2.imwrite(os.path.join(resize_folder, file_name), img)

            unique_mask_colors = np.unique(self.mask).shape[0]
            img, mask = resize(self.pattern, self.mask, scale)
            self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_random_crop(self):
        crop_folder = os.path.join(self.result_folder, 'cropping')
        if self.save_files:
            create_folder(crop_folder)

        img, mask = random_crop(self.pattern, self.mask_gray, 34)
        self.assertEqual(np.unique(mask).shape[0], 2)

        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertTrue(img.shape[0], 34)
        self.assertEqual(img.shape[0], img.shape[1])
        self.assertEqual(mask.shape[0], mask.shape[1])
        self.assertEqual(img.shape[0], mask.shape[0])

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = random_crop(self.pattern, self.mask, 180)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        if self.save_files:
            file_name = f'cropped_to_34.png'
            cv2.imwrite(os.path.join(crop_folder, file_name), img)

        img, mask = random_crop(self.pattern, self.mask_gray, 5000)  # should not crop
        self.assertEqual(img.shape[0], self.pattern.shape[0])
        self.assertEqual(img.shape[1], self.pattern.shape[1])

        _, base_mask = random_crop(self.pattern, self.mask_gray, 34)
        masks = []
        for i in range(20):
            img, mask = random_crop(self.pattern, self.mask_gray, 34)
            self.assertEqual(np.unique(mask).shape[0], 2)
            masks.append(mask)

        results = []
        for mask in masks:
            results.append(np.array_equal(base_mask, mask))

        try:
            self.assertIn(False, results)
        except AssertionError:
            self.fail(
                'Failed during random crop. Each crop was exactly the same. This is very unlikely for 20 random crops.'
            )

    def test_tilting(self):
        tilt_folder = os.path.join(self.result_folder, 'tilting')
        if self.save_files:
            create_folder(tilt_folder)

        # first standard
        img, mask = tilt_forward(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_forward_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_forward_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = tilt_forward(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        img, mask = tilt_backwards(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_backward_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_backward_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = tilt_backwards(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        img, mask = tilt_left(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_left_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_left_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = tilt_left(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        img, mask = tilt_right(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_right_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_right_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = tilt_right(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        # now a little more advanced
        img, mask = tilt_forward(self.pattern, self.mask_gray, start_factor=0)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_1_forward_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_1_forward_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

        img, mask = tilt_backwards(self.pattern, self.mask_gray, end_factor=1)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_1_backward_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_1_backward_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

        img, mask = tilt_left(self.pattern, self.mask_gray, start_factor=0)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_1_left_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_1_left_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

        img, mask = tilt_right(self.pattern, self.mask_gray, end_factor=1)
        self.assertEqual(np.unique(mask).shape[0], 2)
        self.assertEqual(img.shape[0], mask.shape[0])
        self.assertEqual(img.shape[1], mask.shape[1])
        if self.save_files:
            file_name = 'tilt_1_right_img.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), img)
            file_name = 'tilt_1_right_mask.png'
            cv2.imwrite(os.path.join(tilt_folder, file_name), mask)

    def test_distortions(self):
        distort_folder = os.path.join(self.result_folder, 'distort_folder')
        if self.save_files:
            create_folder(distort_folder)

        img, mask = elastic_distortion(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'elastic_img.png'
            cv2.imwrite(os.path.join(distort_folder, file_name), img)
            file_name = 'elastic_mask.png'
            cv2.imwrite(os.path.join(distort_folder, file_name), mask)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = elastic_distortion(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        img, mask = grid_distortion(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        if self.save_files:
            file_name = 'grid_img.png'
            cv2.imwrite(os.path.join(distort_folder, file_name), img)
            file_name = 'grid_mask.png'
            cv2.imwrite(os.path.join(distort_folder, file_name), mask)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = grid_distortion(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

        img, mask = optical_distortion(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)
        if self.save_files:
            file_name = 'optical_img.png'
            cv2.imwrite(os.path.join(distort_folder, file_name), img)
            file_name = 'optical_mask.png'
            cv2.imwrite(os.path.join(distort_folder, file_name), mask)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = optical_distortion(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_random_grid_shuffle(self):
        folder = os.path.join(self.result_folder, 'random_grid_shuffle')
        if self.save_files:
            create_folder(folder)

        img, mask = random_grid_shuffle(self.pattern, self.mask_gray)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'cutout.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

        unique_mask_colors = np.unique(self.mask).shape[0]
        img, mask = random_grid_shuffle(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], unique_mask_colors)

    def test_flips(self):

        base_image = np.zeros((10, 6), dtype=np.uint8)
        base_image[0, 0] = 1

        flipped_x, mask = flip_x(base_image, base_image)
        self.assertTrue(flipped_x[0, 5] == 1)
        self.assertEqual(len(np.unique(mask)), 2)

        flipped_y, mask = flip_y(base_image, base_image)
        self.assertTrue(flipped_y[9, 0] == 1)
        self.assertEqual(len(np.unique(mask)), 2)

        flipped_xy, mask = flip_xy(base_image, base_image)
        self.assertTrue(flipped_xy[9, 5] == 1)
        self.assertEqual(len(np.unique(mask)), 2)
