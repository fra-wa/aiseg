from aiseg.dl_models import VNet
from aiseg.dl_models import VNetLight
from aiseg.tests.model_base_tests import ModelBaseTests3D


class VNetTests(ModelBaseTests3D):
    def test_forward(self):
        self.start_test('VNet', VNet)


class VNetLightTests(ModelBaseTests3D):
    def test_forward(self):
        self.start_test('VNetLight', VNetLight)
