from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import LEDNet


class LEDNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('LEDNet', LEDNet)
