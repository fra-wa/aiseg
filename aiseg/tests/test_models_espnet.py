from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import ESPNet


class ESPNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('ESPNet', ESPNet)
