from aiseg.dl_models import DenseVoxelNet
from aiseg.tests.model_base_tests import ModelBaseTests3D


class DenseVoxelNetTests(ModelBaseTests3D):
    def test_forward(self):
        self.start_test('DenseVoxelNet', DenseVoxelNet)
