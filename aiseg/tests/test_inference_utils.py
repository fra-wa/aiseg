import numpy as np

from unittest import TestCase

from aiseg.inference.utils import get_logits_weight


class InferenceUtilsTests(TestCase):
    def setUp(self) -> None:
        pass

    def test_get_logits_weight(self):

        h = 4
        w = 4
        d = 2
        expected = np.ones((h, w))
        logits = get_logits_weight('sum', h, w)
        self.assertTrue(np.array_equal(logits, expected))
        expected = np.ones((d, h, w))
        logits = get_logits_weight('sum', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))

        expected = np.array([
            [1, 1, 1, 1],
            [1, 2, 2, 1],
            [1, 2, 2, 1],
            [1, 1, 1, 1],
        ])
        logits = get_logits_weight('linear', h, w)
        self.assertTrue(np.array_equal(logits, expected))

        expected = np.array([
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
        ])
        logits = get_logits_weight('linear', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))

        d = 4
        expected = np.array([
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
            [
                [2, 2, 2, 2],
                [2, 4, 4, 2],
                [2, 4, 4, 2],
                [2, 2, 2, 2],
            ],
            [
                [2, 2, 2, 2],
                [2, 4, 4, 2],
                [2, 4, 4, 2],
                [2, 2, 2, 2],
            ],
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
        ])
        logits = get_logits_weight('linear', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))

        expected = np.array(
            [[50, 68, 68, 50],
             [68, 93, 93, 68],
             [68, 93, 93, 68],
             [50, 68, 68, 50]]
        )
        logits = get_logits_weight('gauss', h, w, gauss_weight=1/2)
        self.assertTrue(np.array_equal(logits, expected))

        d = 4
        expected = np.array([
            [
                [50, 61, 61, 50],
                [61, 75, 75, 61],
                [61, 75, 75, 61],
                [50, 61, 61, 50],
            ],
            [
                [61, 75, 75, 61],
                [75, 93, 93, 75],
                [75, 93, 93, 75],
                [61, 75, 75, 61],
            ],
            [
                [61, 75, 75, 61],
                [75, 93, 93, 75],
                [75, 93, 93, 75],
                [61, 75, 75, 61],
            ],
            [
                [50, 61, 61, 50],
                [61, 75, 75, 61],
                [61, 75, 75, 61],
                [50, 61, 61, 50],
            ],
        ])
        logits = get_logits_weight('gauss', h, w, depth=d, gauss_weight=1/2)
        self.assertTrue(np.array_equal(logits, expected))

        d = 4
        expected = np.array([
            [
                [25, 38, 38, 25],
                [38, 57, 57, 38],
                [38, 57, 57, 38],
                [25, 38, 38, 25],
            ],
            [
                [38, 57, 57, 38],
                [57, 86, 86, 57],
                [57, 86, 86, 57],
                [38, 57, 57, 38],
            ],
            [
                [38, 57, 57, 38],
                [57, 86, 86, 57],
                [57, 86, 86, 57],
                [38, 57, 57, 38],
            ],
            [
                [25, 38, 38, 25],
                [38, 57, 57, 38],
                [38, 57, 57, 38],
                [25, 38, 38, 25],
            ],
        ])
        logits = get_logits_weight('gauss', h, w, depth=d, gauss_weight=1/4)
        self.assertTrue(np.array_equal(logits, expected))
