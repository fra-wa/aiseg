from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import DeepLabV3Plus


class DeepLabV3PlusTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DeepLabV3Plus', DeepLabV3Plus, backbone='resnet101')
