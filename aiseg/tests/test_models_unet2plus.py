from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import UNet2Plus


class UNet2PlusTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('UNet2Plus', UNet2Plus)
