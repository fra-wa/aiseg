import numpy as np
import os

from unittest import TestCase

from aiseg.file_handling.utils import create_folder, remove_dir
from aiseg.inference.predictors.image_predictor import SlidingImageCalculator
from aiseg.inference.predictors.volume_predictor import SlidingVolumeCalculator


class InferenceUtilsTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.tmp_logits_dir = os.path.join(files_path, 'logits')
        create_folder(self.tmp_logits_dir)

    def tearDown(self) -> None:
        remove_dir(self.tmp_logits_dir)

    def test_sliding_window(self):
        sliding_window = SlidingImageCalculator(classes=1, height=6, width=6)
        logits = np.ones((1, 4, 4))

        sliding_window.add_current_logits(logits, 0, 0, padding=0)
        sliding_window.add_current_logits(logits, 6-4, 0, padding=0)
        sliding_window.add_current_logits(logits, 6-4, 6-4, padding=0)
        sliding_window.add_current_logits(logits, 0, 6-4, padding=0)

        sub_logits_weight = np.array(
            [[13, 31, 31, 13],
             [31, 79, 79, 31],
             [31, 79, 79, 31],
             [13, 31, 31, 13]]
        )
        expected = 79 + 31 + 31 + 13
        self.assertEqual(expected, sliding_window.logits_image[0, 2, 2])
        self.assertEqual(expected, sliding_window.weighting_image[0, 2, 2])
        out_logits = sliding_window.get_avg_logits_volume()
        expected = np.ones_like(out_logits)
        self.assertTrue(np.array_equal(expected, out_logits))

        sliding_window = SlidingVolumeCalculator(1, depth=6, height=6, width=6)
        logits = np.ones((1, 4, 4, 4))

        sliding_window.add_current_logits(logits, 0, 0, 0, padding=0)
        sliding_window.add_current_logits(logits, 6 - 4, 6 - 4, 6 - 4, padding=0)

        sliding_window.add_current_logits(logits, 6 - 4, 0, 0, padding=0)
        sliding_window.add_current_logits(logits, 0, 6 - 4, 0, padding=0)
        sliding_window.add_current_logits(logits, 0, 0, 6 - 4, padding=0)

        sliding_window.add_current_logits(logits, 6 - 4, 6 - 4, 0, padding=0)
        sliding_window.add_current_logits(logits, 0, 6 - 4, 6 - 4, padding=0)
        sliding_window.add_current_logits(logits, 6 - 4, 0, 6 - 4, padding=0)

        sub_logits_weight = np.array([
            [[12, 23, 23, 12],
             [23, 43, 43, 23],
             [23, 43, 43, 23],
             [12, 23, 23, 12]],
            [[23, 43, 43, 23],
             [43, 79, 79, 43],
             [43, 79, 79, 43],
             [23, 43, 43, 23]],
            [[23, 43, 43, 23],
             [43, 79, 79, 43],
             [43, 79, 79, 43],
             [23, 43, 43, 23]],
            [[12, 23, 23, 12],
             [23, 43, 43, 23],
             [23, 43, 43, 23],
             [12, 23, 23, 12]],
        ])

        expected = 79 + 43 + 43 + 23 + 43 + 23 + 23 + 12
        self.assertEqual(expected, sliding_window.logits_volume[0, 2, 2, 2])
        self.assertEqual(expected, sliding_window.weighting_volume[0, 2, 2, 2])
        out_logits = sliding_window.get_avg_logits_volume()
        expected = np.ones_like(out_logits)
        self.assertTrue(np.array_equal(expected, out_logits))







