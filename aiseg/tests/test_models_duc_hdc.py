from aiseg.tests.model_base_tests import ModelBaseTests2D
from aiseg.dl_models import DeepLabDUCHDC


class DeepLabDUCHDCTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DeepLab_DUC_HDC', DeepLabDUCHDC)
