"""
Source code: cleaned version of yassouali https://github.com/yassouali/pytorch-segmentation

Loss overview:
https://arxiv.org/pdf/2006.14822.pdf
See page 5 for what to use!
"""
import numpy as np
import torch
import torch.nn.functional as F


def make_one_hot(labels, classes):
    one_hot = torch.FloatTensor(labels.size()[0], classes, labels.size()[2], labels.size()[3]).zero_().to(labels.device)
    target = one_hot.scatter_(1, labels.data, 1)
    return target


def get_weights(target):
    t_np = target.view(-1).data.cpu().numpy()

    classes, counts = np.unique(t_np, return_counts=True)
    cls_w = np.median(counts) / counts
    # cls_w = class_weight.compute_class_weight('balanced', classes, t_np)

    weights = np.ones(7)
    weights[classes] = cls_w
    return torch.from_numpy(weights).float().cuda()


class CrossEntropyLoss(torch.nn.Module):
    def __init__(self, binary, weight=None, **kwargs):
        super(CrossEntropyLoss, self).__init__()
        if not isinstance(binary, bool):
            raise ValueError('Binary must be a bool.')
        if binary:
            self.loss = torch.nn.BCEWithLogitsLoss(weight=weight, **kwargs)
        else:
            self.loss = torch.nn.CrossEntropyLoss(weight=weight, **kwargs)

    def forward(self, output, target):
        loss = self.loss(output, target)
        return loss


class DiceLoss(torch.nn.Module):
    def __init__(self, smooth=1):
        super(DiceLoss, self).__init__()
        self.smooth = smooth

    def forward(self, output, target):
        target = make_one_hot(target.unsqueeze(dim=1), classes=output.size()[1])
        output = F.softmax(output, dim=1)
        output_flat = output.contiguous().view(-1)
        target_flat = target.contiguous().view(-1)
        intersection = (output_flat * target_flat).sum()
        loss = 1 - ((2 * intersection + self.smooth) / (output_flat.sum() + target_flat.sum() + self.smooth))
        return loss


class FocalLoss(torch.nn.Module):
    def __init__(self, binary, gamma=2, weight=None):
        super(FocalLoss, self).__init__()
        if not isinstance(binary, bool):
            raise ValueError('Binary must be a bool.')

        self.gamma = gamma
        self.ce_loss = CrossEntropyLoss(binary, weight=weight, reduce=False)

    def forward(self, output, target):
        ce_loss = self.ce_loss(output, target)
        pt = torch.exp(-ce_loss)
        loss = ((1 - pt) ** self.gamma) * ce_loss
        return loss.sum()


class CrossEntropyDiceLoss(torch.nn.Module):
    def __init__(self, binary, smooth=1, weight=None):
        super(CrossEntropyDiceLoss, self).__init__()
        if not isinstance(binary, bool):
            raise ValueError('Binary must be a bool.')

        self.dice = DiceLoss(smooth=smooth)
        self.cross_entropy = CrossEntropyLoss(binary, weight)

    def forward(self, output, target):
        ce_loss = self.cross_entropy(output, target)
        dice_loss = self.dice(output, target)
        return ce_loss + dice_loss
