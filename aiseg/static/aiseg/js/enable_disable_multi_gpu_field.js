window.addEventListener('load', function load(event) {
    let device_select = document.getElementById("id_device");
    let num_gpus_field = document.getElementById("id_num_gpus");

    let init_num_gpus = num_gpus_field.value;

    if (device_select.options.length === 3) {
        device_select.options[device_select.options.length - 1].setAttribute('disabled', '');
    }

    if (device_select.value !== "cuda") {
        if (!num_gpus_field.hasAttribute("disabled")) {
            num_gpus_field.setAttribute('disabled', '');
            num_gpus_field.value = null;
        }
    }

    device_select.addEventListener("change", function() {
        if (device_select.value === "cuda") {
            if (num_gpus_field.hasAttribute("disabled")) {
                num_gpus_field.removeAttribute('disabled');
                num_gpus_field.value = init_num_gpus;
            }
        } else {
            if (!num_gpus_field.hasAttribute("disabled")) {
                num_gpus_field.setAttribute('disabled', '');
                num_gpus_field.value = null;
            }
        }
    })
});
