var backbone_choices;
var epochs_field;
var lr_warm_up_period_field;
var lr_constant_period_field;
var fine_tuning_epoch_field;
var lr_scheduler_field;
var lr_decay_field;
var lr_adjust_step_size_field;
var previous_normalization_select = null;

function updateSuccessModalContent(data) {
    let parametersText = document.getElementById('js-calculated-parameters-text');
    let ramInfoText = document.getElementById('js-ram-info-text');

    if (parametersText) {
        parametersText.innerHTML = `
            Using your current parameters, the following values are calculated:<br>
            Input Size: ${data["form_data"]["input_size"]} x ${data["form_data"]["input_size"]}<br>
            ${data["form_data"]["input_depth"] ? `Input depth: ${data["form_data"]["input_depth"]}<br>` : ''}
            Batch Size: ${data["form_data"]["batch_size"]}<br>
        `;
    }
    if (ramInfoText) {
        ramInfoText.innerHTML = `Approximated RAM usage during training: ${data["used_ram"]} ${data["ram_unit"]}`;
    }
}

function showErrors(errors) {
    if (errors) {
        errors = JSON.parse(errors);
    } else {
        return
    }

    for (let fieldName in errors) {
        if (errors.hasOwnProperty(fieldName)) {
            let fieldId = 'id_' + fieldName;
            let fieldElement = document.getElementById(fieldId);

            if (fieldElement) {
                // Find the closest .form-group ancestor
                let formGroup = fieldElement.closest('.form-group');

                if (formGroup) {
                    let fieldErrors = errors[fieldName];

                    fieldErrors.forEach(error => {
                        let errorDiv = document.createElement('div');
                        errorDiv.classList.add('alert', 'alert-danger');
                        errorDiv.setAttribute('role', 'alert');
                        errorDiv.textContent = error["message"];

                        formGroup.appendChild(errorDiv);
                    });
                }
            }
        }
    }
    const architectureField = document.getElementById('id_architecture');
    architectureField.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

function updateFormFields(formData) {
    // Retrieve the --teal-dark color from the CSS
    const fieldBackgroundSuccess = getComputedStyle(document.documentElement).getPropertyValue('--success-green').trim();

    // Update form fields with the calculated values
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            let fieldId = 'id_' + key;
            let fieldElement = document.getElementById(fieldId);

            if (fieldElement) {
                if ((formData[key] === null)) {
                    continue
                }
                fieldElement.value = formData[key];
                fieldElement.style.backgroundColor = fieldBackgroundSuccess;
                fieldElement.title = 'Calculated value';

                // Add event listeners only if not already added
                if (!fieldElement.hasAttribute('color-listeners-added')) {
                    // reset the background color and tooltip on input and focus
                    fieldElement.addEventListener('focus', function() {
                        fieldElement.style.backgroundColor = '';
                        fieldElement.title = '';
                    });

                    fieldElement.addEventListener('input', function() {
                        fieldElement.style.backgroundColor = '';
                        fieldElement.title = '';
                    });

                    // Mark the element as having the listeners added
                    fieldElement.setAttribute('color-listeners-added', 'true');
                }
            }
        }
    }
}

function showMessages(messages) {
    if (messages) {
        let messageContainer = document.getElementById('id_messages_container');

        if (!messageContainer) {
            messageContainer = document.createElement('ul');
            messageContainer.id = 'id_messages_container';
            messageContainer.classList.add('messages');
            messageContainer.style.paddingLeft = '0';

            const mainContainer = document.querySelector('.container-main');
            if (mainContainer) {
                mainContainer.insertBefore(messageContainer, mainContainer.firstChild);
            }
        } else {
            // Clear previous messages
            messageContainer.innerHTML = '';
        }

        let firstAlertDiv = null;

        messages.forEach(([level, message], index) => {
            let alertDiv = document.createElement('div');

            if (level === 20) {
                alertDiv.className = 'alert alert-custom-warning p-2';
            } else if (level === 30) {
                alertDiv.className = 'alert alert-danger p-2';
            } else {
                alertDiv.className = 'alert alert-secondary p-2';
            }

            alertDiv.setAttribute('role', 'alert');
            alertDiv.innerHTML = message;
            messageContainer.appendChild(alertDiv);

            if (index === 0) {
                firstAlertDiv = alertDiv;
            }

        });

        if (firstAlertDiv) {
            firstAlertDiv.scrollIntoView({ behavior: 'smooth', block: 'center' });
        }
    }
}

function clearAllErrors() {
    // Select and remove only elements with the exact classes 'alert' and 'alert-danger' and role 'alert'
    document.querySelectorAll('div.alert.alert-danger[role="alert"]').forEach(errorDiv => {
        errorDiv.remove();
    });
}

function postCalculationRequest() {
    let form = document.getElementById('input_form');
    let waitModalElement = document.getElementById('waitModal');
    let successModalElement = document.getElementById('js-ram-calc-success-modal');
    let waitModal = new bootstrap.Modal(waitModalElement, {
        backdrop: 'static',
        keyboard: false
    });
    let successModal = new bootstrap.Modal(successModalElement, {
        backdrop: 'static',
        keyboard: false
    });

    clearAllErrors();

    waitModal.show();

    let formData = new FormData(form);
    formData.append('calculate_optimal_parameters', 'true');

    // Post ajax data
    fetch(form.action, {
        method: 'POST',
        body: formData,
        headers: {
            'X-Requested-With': 'XMLHttpRequest' // indicates AJAX request for backend
        }
    }).then(response => response.json()).then(data => {
        // Hide the wait modal and show the success modal if no errors are contained
        waitModal.hide();
        if (data.success) {
            updateFormFields(data.data['form_data']);
            updateSuccessModalContent(data.data);
            successModal.show();

            // Use Bootstrap's event hook to perform actions after the modal is shown
            const onShow = () => {
                const inputSizeField = document.getElementById('id_input_size');
                if (inputSizeField) {
                    inputSizeField.scrollIntoView({ behavior: 'smooth', block: 'center' });
                }
                // Remove the event listener after execution
                successModalElement.removeEventListener('shown.bs.modal', onShow);
            };

            // Add event listener with the `once` option
            successModalElement.addEventListener('shown.bs.modal', onShow, { once: true });

        } else {
            showErrors(data.form_errors);
            showMessages(data.data['messages']);
        }
    }).catch(error => {
        console.error('There was a problem with the fetch operation:', error);
        waitModal.hide();
        successModal.hide();
    }).finally(() => {});
}

function updateBackboneSelect() {
    let architecture_select = document.getElementById("id_architecture");
    let backbone_select = document.getElementById("id_backbone");
    let initial_value = backbone_select.value;
    backbone_select.length = 0;

    let selected_architecture = architecture_select.options[architecture_select.selectedIndex].value;
    let backbone_list = backbone_choices[selected_architecture];

    for (let i = 0; i < backbone_list.length; i++) {
        let option = document.createElement("option");
        option.value = backbone_list[i];
        if (option.value === initial_value) {
            option.setAttribute('selected', '')
        }
        if (backbone_list[i]){
            option.text = backbone_list[i];
        } else {
            option.text = '-------------';
        }
        backbone_select.appendChild(option);
    }
}

function updateRequestButton(blocked_devices) {
    let device_select = document.getElementById("id_device");
    let selected_device = device_select.value;
    let request_button = document.getElementById("js-optimal-para-request-button");
    let calc_button_text = document.getElementsByClassName('js-data-holder')[0].dataset.calc_button_text;
    let calc_button_blocked_text = document.getElementsByClassName('js-data-holder')[0].dataset.calc_button_blocked_text;

    if (blocked_devices.includes(selected_device)) {
        request_button.innerHTML = calc_button_blocked_text;
        if (request_button.className.split(' ').includes('btn-primary-form')) {
            request_button.className = request_button.className.replace('btn-primary-form', 'btn-log-running');
        }
    } else {
        request_button.innerHTML = calc_button_text;
        if (request_button.className.split(' ').includes('btn-log-running')) {
            request_button.className = request_button.className.replace('btn-log-running', 'btn-primary-form');
        }
    }
}

function set_disabled(field) {
    if (!field.hasAttribute("disabled")) {
        field.setAttribute('disabled', '');
    }
}

function set_active(field) {
    if (field.hasAttribute("disabled")) {
        field.removeAttribute('disabled');
    }
}

function update_lr_scheduling_fields(is_scheduler_change=false, is_epoch_update=false) {
    if (lr_scheduler_field.value === 'LinearLR') {
        set_active(lr_constant_period_field);
        set_active(lr_warm_up_period_field);
        set_disabled(lr_adjust_step_size_field);

        if (is_scheduler_change) {
            lr_constant_period_field.value = epochs_field.value;
            lr_warm_up_period_field.value = "0";
            lr_adjust_step_size_field.value = "0";
        }

        lr_decay_field.value = parseInt(epochs_field.value) - (parseInt(lr_warm_up_period_field.value) + parseInt(lr_constant_period_field.value));
        if (lr_decay_field.value < 0) {
            lr_decay_field.style.backgroundColor = "var(--danger-red)";
            lr_decay_field.style.color = "var(--gray-light)";
        } else {
            lr_decay_field.style.backgroundColor = "";
            lr_decay_field.style.color = "";
        }
    } else {
        if (is_scheduler_change) {
            lr_decay_field.value = '0';
            lr_constant_period_field.value = epochs_field.value;
            lr_warm_up_period_field.value = "0";
        } else if (is_epoch_update) {
            lr_constant_period_field.value = epochs_field.value;
        }

        lr_decay_field.style.backgroundColor = "";
        lr_decay_field.style.color = "";
        set_active(lr_adjust_step_size_field);
        set_disabled(lr_constant_period_field);
        set_disabled(lr_warm_up_period_field);
    }
}

function validate_fine_tuning_epoch() {
    let epochs_field = document.getElementById("id_epochs");
    let fine_tuning_field = document.getElementById("id_fine_tuning_epoch");

    if (parseInt(fine_tuning_field.value) > parseInt(epochs_field.value)) {
        fine_tuning_field.style.backgroundColor = "var(--danger-red)";
        fine_tuning_field.style.color = "var(--gray-light)";
    } else {
        fine_tuning_field.style.backgroundColor = "";
        fine_tuning_field.style.color = "";
    }
}

function updateNormLayerSelect() {
    let norm_select = document.getElementById("id_normalization");
    let architecture_select = document.getElementById("id_architecture");
    let selected_architecture = architecture_select.options[architecture_select.selectedIndex].value;
    previous_normalization_select = norm_select.value;

    if (selected_architecture === 'SwinUnet') {
        let layerNormOption = Array.from(norm_select.options).find(option => option.value === 'ln');
        if (!layerNormOption.selected) {
            layerNormOption.selected = true;
        }
        norm_select.title = "SwinUnet only allows Layer normalization";
        norm_select.style.backgroundColor = 'var(--danger-red)';
        norm_select.style.color = 'white';
        set_disabled(norm_select);

    } else {
        norm_select.title = "";
        norm_select.style.backgroundColor = '';
        norm_select.style.color = '';

        set_active(norm_select);

        // Restore the previously selected value if it exists
        if (previous_normalization_select !== null) {
            let previousOption = Array.from(norm_select.options).find(option => option.value === previous_normalization_select);
            if (previousOption) {
                previousOption.selected = true;
                norm_select.value = previous_normalization_select;
            }
        } else {
            // If there is no previous value, set a default option (e.g., 'bn')
            let defaultOption = Array.from(norm_select.options).find(option => option.value === 'bn');
            if (defaultOption) {
                defaultOption.selected = true;
                norm_select.value = 'bn';
            }
        }
    }
}


window.addEventListener('load', function load(event) {
    let request_button = document.getElementById("js-optimal-para-request-button");
    let architecture_select = document.getElementById("id_architecture");
    let device_select = document.getElementById("id_device");
    backbone_choices = JSON.parse(document.getElementsByClassName('js-data-holder')[0].dataset.backbone_dict);
    let blocked_devices = document.getElementsByClassName('js-data-holder')[0].dataset.blocked_devices;
    blocked_devices = blocked_devices.replace(/ /g, '').replace(/'/g, '').replace(/\[/g, '').replace(/]/g, '').split(',')

    epochs_field = document.getElementById("id_epochs");
    lr_warm_up_period_field = document.getElementById("id_lr_warm_up_period");
    lr_constant_period_field = document.getElementById("id_lr_constant_period");
    fine_tuning_epoch_field = document.getElementById("id_fine_tuning_epoch");
    lr_scheduler_field = document.getElementById("id_lr_scheduler");
    lr_decay_field = document.getElementById("id_lr_decay_period");
    lr_adjust_step_size_field = document.getElementById("id_lr_adjust_step_size");

    update_lr_scheduling_fields();
    updateNormLayerSelect();

    updateBackboneSelect();
    updateRequestButton(blocked_devices);

    request_button.addEventListener("click", function () {
        postCalculationRequest();
    });

    architecture_select.addEventListener("change", function () {
        updateBackboneSelect();
        updateNormLayerSelect();
    });

    device_select.addEventListener("change", function () {
        updateRequestButton(blocked_devices);
    });

    epochs_field.addEventListener("change", function() {
        update_lr_scheduling_fields(false, true);
        validate_fine_tuning_epoch();
    });

    lr_warm_up_period_field.addEventListener("change", function() {
        update_lr_scheduling_fields();
    });

    lr_constant_period_field.addEventListener("change", function() {
        update_lr_scheduling_fields();
    });

    fine_tuning_epoch_field.addEventListener("change", function() {
        validate_fine_tuning_epoch();
    });

    lr_scheduler_field.addEventListener("change", function() {
        update_lr_scheduling_fields(true, false);
    });

    // To successfully sent the potentially disabled norm_select data:
    // disabled fields will not be posted to the backend. --> handled by form_submit_helper.js

});
