VANTA.NET({
    el: "#net-background",
    mouseControls: false,
    touchControls: false,
    gyroControls: false,
    minHeight: 200.00,
    minWidth: 200.00,
    scale: 1.00,
    scaleMobile: 1.00,
    color: 0x9dc1cf,
    backgroundColor: 0x080f19,
    maxDistance: 26.00,
    spacing: 16.00
})
