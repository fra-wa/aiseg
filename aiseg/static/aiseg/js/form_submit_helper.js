window.addEventListener('load', function load(event) {
    document.addEventListener('submit', function (event) {
        const form = event.target;

        // Enable disabled fields in the form -> otherwise they are not sent
        const disabledFields = form.querySelectorAll('input:disabled, textarea:disabled, select:disabled');
        disabledFields.forEach(field => {
            field.disabled = false;
        });

        // Disable all submit buttons to prevent double submits:

        // When disabling buttons, the name and value of a button will not be appended to the POST.
        // Workaround: create hidden input field with the buttons name and value.
        const clickedButton = form.querySelector('button[type="submit"]:focus, input[type="submit"]:focus');

        if (clickedButton && clickedButton.name) {
            const hiddenInput = document.createElement('input');
            hiddenInput.type = 'hidden';
            hiddenInput.name = clickedButton.name;
            hiddenInput.value = clickedButton.value;
            form.appendChild(hiddenInput);
        }

        // Disable submit buttons
        const allSubmitButtons = document.querySelectorAll('button[type="submit"], input[type="submit"]');
        allSubmitButtons.forEach(button => {
            button.disabled = true;
        });

        // Also disable all anchors with role=button -> prevents from clicking back during a submission and
        // re-submitting the same
        // Anchors cannot be disabled -> changing their style and disabling the links should do the trick
        const allAnchorButtons = document.querySelectorAll('a[role="button"]');
        allAnchorButtons.forEach(anchor => {
            // Make the anchor visually appear disabled
            anchor.style.pointerEvents = 'none';
            anchor.style.opacity = '0.6';
            anchor.setAttribute('aria-disabled', 'true');
        });
    });

    // When clicking back (mouse or back), buttons are still disabled - must be activated again
    // Add a listener for the pageshow event to re-enable buttons and anchors
    window.addEventListener('pageshow', function () {
        const allSubmitButtons = document.querySelectorAll('button[type="submit"], input[type="submit"]');
        allSubmitButtons.forEach(button => {
            button.disabled = false;
        });

        const allAnchorButtons = document.querySelectorAll('a[role="button"]');
        allAnchorButtons.forEach(anchor => {
            anchor.style.pointerEvents = 'auto';
            anchor.style.opacity = '1';
            anchor.removeAttribute('aria-disabled');
        });
    });
});