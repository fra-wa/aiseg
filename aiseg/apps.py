from django.apps import AppConfig


class AiSegConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aiseg'
    verbose_name = "AiSeg"
