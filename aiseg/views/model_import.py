import logging
import os
import time
import traceback
import torch

from decimal import Decimal

from django.shortcuts import redirect
from django.utils import timezone

from aiseg import constants
from aiseg.forms.model_import import ModelImportForm
from aiseg.models import TrainingDataset, ModelAnalysis
from aiseg.utils import get_human_datetime, get_time_spent_string, kill_children_and_log_help_info
from aiseg.views.utils import start_process_and_create_log_file
from aiseg.views.utils import aiseg_render as render


def add_model(checkpoints_to_load, training_dataset, device='cpu'):

    device = torch.device(device)

    for i, checkpoint_path in enumerate(checkpoints_to_load):
        logging.info(f'Importing model {i+1}/{len(checkpoints_to_load)}')
        checkpoint = torch.load(checkpoint_path, map_location=device)
        checkpoint_keys = checkpoint.keys()
        history = checkpoint['history']

        backbone = ''
        input_size = 0
        input_depth = 0
        batch_size = 0

        if 'backbone' in checkpoint_keys:
            backbone = checkpoint['backbone']
        if 'batch_size' in checkpoint_keys:
            batch_size = checkpoint['batch_size']
        if 'image_input_size' in checkpoint_keys:
            input_size = checkpoint['image_input_size']
        if 'depth_3d' in checkpoint_keys or 'input_depth' in checkpoint_keys:
            try:
                input_depth = checkpoint['depth_3d']
            except KeyError:
                input_depth = checkpoint['input_depth']

        best_epoch = sorted(history, key=lambda x: x[2])[0]
        training_loss = Decimal(best_epoch[1])
        validation_loss = Decimal(best_epoch[2])
        training_accuracy = Decimal(round(best_epoch[3], 4))
        validation_accuracy = Decimal(round(best_epoch[4], 4))

        if checkpoint['online_aug']:
            online_aug_kwargs_string = str(checkpoint['online_aug_kwargs'])
        else:
            online_aug_kwargs_string = str({})

        # fix for old checkpoints.
        if checkpoint['architecture'] == 'UperNet':
            checkpoint['architecture'] = constants.UPerNet_NAME
            torch.save(checkpoint, checkpoint_path)

        normalization = ''
        if 'normalization' in checkpoint_keys:
            normalization = constants.NORMALIZATION_DICT[checkpoint['normalization']]

        training_started_time = timezone.now()
        if 'training_started_time' in checkpoint_keys:
            training_started_time = checkpoint['training_started_time']

        analyses = ModelAnalysis.objects.filter(
            created_datetime=training_started_time,
            architecture=checkpoint['architecture'],
            backbone=backbone,
            dataset=training_dataset,
            total_epochs=history[-1][0],
            batch_size=batch_size,
            channels=checkpoint['image_channels'],
            classes=checkpoint['classes'],
            input_height=input_size,
            input_width=input_size,
            input_depth=input_depth,
            online_aug_kwargs_string=online_aug_kwargs_string,
            best_epoch=best_epoch[0],
            training_loss=round(training_loss, 5),
            validation_loss=round(validation_loss, 5),
            training_accuracy=round(training_accuracy, 5),
            validation_accuracy=round(validation_accuracy, 5),
            storage_size=Decimal(round(os.stat(checkpoint_path).st_size / 1000 / 1000, 2)),
            normalization=normalization,
            custom_name='',
        )

        if analyses.count():
            logging.info(
                f'Model seems to already exist. Give a custom name to the existing one to import the new one.'
            )
            logging.info(f'Model info: created_datetime={training_started_time}')
            logging.info(f'Model info: architecture={checkpoint["architecture"]}')
            logging.info(f'Model info: backbone={backbone}')
            logging.info(f'Model info: dataset={training_dataset}')
            logging.info(f'Model info: total_epochs={history[-1][0]}')
            logging.info(f'Model info: batch_size={batch_size}')
            logging.info(f'Model info: channels={checkpoint["image_channels"]}')
            logging.info(f'Model info: classes={checkpoint["classes"]}')
            logging.info(f'Model info: input_height={input_size}')
            logging.info(f'Model info: input_width={input_size}')
            logging.info(f'Model info: input_depth={input_depth}')
            logging.info(f'Model info: online_aug_kwargs_string={online_aug_kwargs_string}')
        else:
            model_analysis = ModelAnalysis.objects.create(
                created_datetime=training_started_time,
                architecture=checkpoint['architecture'],
                backbone=backbone,
                dataset=training_dataset,
                total_epochs=history[-1][0],
                batch_size=batch_size,
                channels=checkpoint['image_channels'],
                classes=checkpoint['classes'],
                input_height=input_size,
                input_width=input_size,
                input_depth=input_depth,
                online_aug_kwargs_string=online_aug_kwargs_string,
                best_epoch=best_epoch[0],
                training_loss=training_loss,
                validation_loss=validation_loss,
                training_accuracy=training_accuracy,
                validation_accuracy=validation_accuracy,
                storage_size=Decimal(round(os.stat(checkpoint_path).st_size / 1000 / 1000, 2)),
                normalization=normalization,
            )
            model_analysis.save_history(history)
            logging.info(f'Model {i + 1}/{len(checkpoints_to_load)} imported!')


def init_model_import(checkpoints_to_load, training_dataset, process_logger):
    process_logger.set_up_root_logger()
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Starting model import at: {get_human_datetime()}')
        add_model(checkpoints_to_load, training_dataset, device='cpu')

        time_spent = get_time_spent_string(start_time)
        logging.info(f'Finished import at: {get_human_datetime()} (time spent: {time_spent})')
    except Exception:
        logging.error(traceback.format_exc())
        kill_children_and_log_help_info()
    finally:
        process_logger.remove_logger_handlers()  # To free resources


def trained_model_import(request):
    form = ModelImportForm()

    if request.method == 'POST':
        form = ModelImportForm(request.POST)

        if form.is_valid():
            fcd = form.cleaned_data

            training_dataset = TrainingDataset.objects.get(pk=fcd['dataset_choice'])
            checkpoints = form.checkpoints

            log_file = start_process_and_create_log_file(
                execution_type=constants.DL_EXECUTION_IMPORT,
                device='cpu',
                process_func=init_model_import,
                process_func_args=[checkpoints, training_dataset]
            )

            return redirect('aiseg:monitor', log_pk=log_file.pk)

    context = {
        'form': form,
        'navbar_view': 'model_import',
    }
    return render(request, 'aiseg/trained_model_import.html', context)
