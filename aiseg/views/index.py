from .utils import aiseg_render as render


def index(request):
    context = {
        'navbar_view': 'index',
    }
    return render(request, 'aiseg/index.html', context)
