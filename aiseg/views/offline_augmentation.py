import logging
import os
import time
import traceback
import sys

# start fix:
# during testing view: with spawn as start method, django cannot set up correctly.
current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(os.path.dirname(current_dir))
sys.path.extend([module_dir])

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

import django
django.setup()
# end fix

from django.shortcuts import redirect

from aiseg import constants
from aiseg.data_augmentation.offline_image_augmenter import ImageAugmenter
from aiseg.data_augmentation.offline_volume_augmenter import VolumeAugmenter
from aiseg.forms.offline_augmentation import OfflineImageAugForm, OfflineVolumeAugForm
from aiseg.models import OfflineImageAugmentationParameters, OfflineVolumeAugmentationParameters
from aiseg.temporary_parameters.parameters import FixedParameters
from aiseg.utils import kill_children_and_log_help_info, get_human_datetime, get_time_spent_string
from aiseg.views.utils import aiseg_render as render
from aiseg.views.utils import get_and_check_running_logs_and_warn
from aiseg.views.utils import kill_running_process_if_needed
from aiseg.views.utils import start_process_and_create_log_file


def init_offline_image_augmentation(parameters, process_logger):
    if not isinstance(parameters, OfflineImageAugmentationParameters):
        raise ValueError('parameters must be an OfflineImageAugmentationParameters instance!')
    if not parameters.images_folder and not parameters.dataset_folder:
        raise ValueError('Need images and masks folder or dataset folder!')

    params = FixedParameters()
    params.images_folder = parameters.images_folder
    params.masks_folder = parameters.masks_folder
    if not params.images_folder:
        params.images_folder = os.path.join(constants.DATASET_FOLDER, parameters.dataset_folder, 'images')
        params.masks_folder = os.path.join(constants.DATASET_FOLDER, parameters.dataset_folder, 'masks')

    params.out_folder = parameters.output_folder if parameters.output_folder else None
    params.min_dimension = parameters.min_dimension
    params.max_augmentations = parameters.max_augmentations
    params.total_augmentations = parameters.total_augmentations

    process_logger.set_up_root_logger()
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Starting image augmentation at: {get_human_datetime()}')

        execute_attrs = [f.name for f in OfflineImageAugmentationParameters._meta.get_fields()]
        execute_attrs = [attr for attr in execute_attrs if attr.startswith('do_')]

        operations_to_skip = []
        for attr_name in execute_attrs:
            if not parameters.__getattribute__(attr_name):
                operations_to_skip.append(attr_name[3:])

        human_augmentations = [
            attr[3:].replace('_', ' ') for attr in execute_attrs if
            attr[3:] not in operations_to_skip
        ]
        logging.info('Parameters:')
        logging.info(f'images_folder: {params.images_folder}')
        logging.info(f'masks_folder: {params.masks_folder}')
        logging.info(f'out_folder: {params.out_folder}')
        logging.info(f'min_dimension: {params.min_dimension}')
        logging.info(f'max_augmentations: {params.max_augmentations}')
        logging.info(f'total_augmentations: {params.total_augmentations}')
        logging.info(f'Augmentations: {", ".join(human_augmentations)}')

        augmenter = ImageAugmenter(
            images_folder=params.images_folder,
            masks_folder=params.masks_folder,
            min_dimension=params.min_dimension,
            max_augmentations_per_image=params.max_augmentations,
            max_rotation_angle=parameters.max_rot_angle,
            out_folder=params.out_folder,
            total_augmentations=parameters.total_augmentations,
            operation_skip_list=operations_to_skip,
            logger=process_logger,
        )
        augmenter.augment()

        time_string = get_time_spent_string(start_time)
        logging.info(f'Finished image augmentation at: {get_human_datetime()} (time spent: {time_string})')
    except Exception:
        logging.error(traceback.format_exc())
        kill_children_and_log_help_info()
    finally:
        process_logger.remove_logger_handlers()  # To free resources


def init_offline_volume_augmentation(parameters, process_logger):
    if not isinstance(parameters, OfflineVolumeAugmentationParameters):
        raise ValueError('parameters must be an OfflineVolumeAugmentationParameters instance!')

    params = FixedParameters()
    params.images_folder = parameters.images_folder
    params.masks_folder = parameters.masks_folder
    params.dataset_folder = os.path.join(constants.DATASET_FOLDER, parameters.dataset_folder)
    params.out_folder = parameters.output_folder
    params.min_dimension_x = parameters.min_dimension_x
    params.min_dimension_y = parameters.min_dimension_y
    params.min_dimension_z = parameters.min_dimension_z
    params.rotation_angle = parameters.rotation_angle
    params.max_augmentations = parameters.max_augmentations
    params.total_augmentations = parameters.total_augmentations

    process_logger.set_up_root_logger()

    params.skip_list = []
    execute_attrs = [f.name for f in OfflineVolumeAugmentationParameters._meta.get_fields()]
    execute_attrs = [attr for attr in execute_attrs if attr.startswith('do_')]

    for attr in execute_attrs:
        if not parameters.__getattribute__(attr):
            params.skip_list.append(attr[3:])

    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Starting volume augmentation at: {get_human_datetime()}')

        human_augmentations = [
            attr[3:].replace('_', ' ') for attr in execute_attrs if
            attr[3:] not in params.skip_list
        ]
        logging.info('Parameters:')
        logging.info(f'images_folder: {params.images_folder}')
        logging.info(f'masks_folder: {params.masks_folder}')
        logging.info(f'dataset_folder: {params.dataset_folder}')
        logging.info(f'out_folder: {params.out_folder}')
        logging.info(f'min_dimension_x: {params.min_dimension_x}')
        logging.info(f'min_dimension_y: {params.min_dimension_y}')
        logging.info(f'min_dimension_z: {params.min_dimension_z}')
        logging.info(f'rotation_angle: {params.rotation_angle}')
        logging.info(f'max_augmentations: {params.max_augmentations}')
        logging.info(f'total_augmentations: {params.total_augmentations}')
        logging.info(f'Augmentations: {", ".join(human_augmentations)}')

        if not params.out_folder:
            raise ValueError('Pass the output folder')

        max_augmentations = params.max_augmentations
        if max_augmentations <= 0:
            params.max_augmentations = None

        if not params.images_folder:
            if not os.path.isdir(params.dataset_folder):
                raise FileNotFoundError(f'Could not find the folder: {params.dataset_folder}')

            params.images_folder = os.path.join(params.dataset_folder, 'images')
            params.masks_folder = os.path.join(params.dataset_folder, 'masks')

        augmenter = VolumeAugmenter(
            images_folder=params.images_folder,
            masks_folder=params.masks_folder,
            min_dimension=[params.min_dimension_z, params.min_dimension_y, params.min_dimension_x],
            max_augmentations_per_volume=params.max_augmentations,
            rotation_angle=params.rotation_angle,
            total_augmentations=params.total_augmentations,
            operation_skip_list=params.skip_list,
            out_folder=params.out_folder,
            logger=process_logger,
        )
        augmenter.augment()

        time_string = get_time_spent_string(start_time)
        logging.info(f'Finished volume augmentation at: {get_human_datetime()} (time spent: {time_string})')

    except Exception:
        logging.error(traceback.format_exc())
        kill_children_and_log_help_info()
    finally:
        process_logger.remove_logger_handlers()


def augmentation(request):
    context = {
        'navbar_view': 'augmentation',
    }
    return render(request, 'aiseg/augmentation/augmentation.html', context)


def image_augmentation(request):
    running_logs = get_and_check_running_logs_and_warn(request)
    parameters = OfflineImageAugmentationParameters.objects.last()
    if parameters is None:
        parameters = OfflineImageAugmentationParameters.objects.create()

    form = OfflineImageAugForm(instance=parameters, request=request)
    if request.method == 'POST':
        form = OfflineImageAugForm(request, request.POST, instance=parameters)
        if form.is_valid():
            if form.has_changed():
                parameters = form.save()

            kill_running_process_if_needed(running_logs=running_logs, device='cpu', request=request)

            log_file = start_process_and_create_log_file(
                execution_type=constants.DL_EXECUTION_AUG_IMG,
                device='cpu',
                process_func=init_offline_image_augmentation,
                parameters=parameters,
            )

            return redirect('aiseg:monitor', log_pk=log_file.pk)

    context = {
        'navbar_view': 'augmentation',
        'form': form,
        'process_is_running': True if running_logs else False,
    }
    return render(request, 'aiseg/augmentation/offline_image_augmentation.html', context)


def volume_augmentation(request):
    running_logs = get_and_check_running_logs_and_warn(request)
    parameters = OfflineVolumeAugmentationParameters.objects.last()
    if parameters is None:
        parameters = OfflineVolumeAugmentationParameters.objects.create()

    form = OfflineVolumeAugForm(request=request, instance=parameters)
    if request.method == 'POST':
        form = OfflineVolumeAugForm(request, request.POST, instance=parameters)
        if form.is_valid():
            if form.has_changed():
                parameters = form.save()

            kill_running_process_if_needed(running_logs=running_logs, device='cpu', request=request)

            log_file = start_process_and_create_log_file(execution_type=constants.DL_EXECUTION_AUG_VOL, device='cpu',
                                                         process_func=init_offline_volume_augmentation,
                                                         parameters=parameters)

            return redirect('aiseg:monitor', log_pk=log_file.pk)

    context = {
        'navbar_view': 'augmentation',
        'form': form,
        'process_is_running': True if running_logs else False,
    }
    return render(request, 'aiseg/augmentation/offline_volume_augmentation.html', context)
